---
layout: post
title:  "Living with Ideas."
date:   2018-12-03 19:45:31 +0530
categories: [term1]
---
![by Gabor Mandoki]({{site.baseurl}}/assets/week8/HeadphonesII.jpg)

# living with Ideas.
---
&nbsp;

### the methodology to imagine the future.

This week, we not only learned how to design, but how to develop ideas. We have been pushed to create worlds, scenarios and objects for hypothetical purposes. First, it felt a bit weird to create concepts for not existing realities. But in the end, you understand that this is one reliable method to face future as the future is a not existing reality.

&nbsp;

### the parallel universe.

So, the challenge was to think in a parallel universe where you have to accomplish the given challenges with a device we were created out of everyday stuff.
In my hypothetical world, humans only could communicate with each other by being physically connected.
My device was an old headphone ripped apart. I connected only the two earpieces with each other. When people wanted to communicate with me, they had to take one side of my machine and connect it to themselves. Now, you could only talk and listen but you could also put the phone to your head to hear the thoughts or to your heart to hear the feelings.

![]({{site.baseurl}}/assets/week8/HeadphonesI.jpg)

The reactions have been very similar. Most of the time the people laughed at first and then tried to speak with me. Because of being literally connected, personal vibe raised immediately. Sometimes I wish to have that option, sitting in a room full of people and only to talk to one, hear the others thoughts or having the ability to switch off.

&nbsp;

### the digital fabric.

How to add a digital interface to an analogue everyday object? Well, in this experiment we reshaped our environment by covering some of it and projected patterns and pictures. We augmented the reality with the help of the green screen technology.
&nbsp;

![]({{site.baseurl}}/assets/week8/IMG_0073.GIF)

![]({{site.baseurl}}/assets/week8/IMG_0074.GIF)

![]({{site.baseurl}}/assets/week8/IMG_0075.GIF)

![]({{site.baseurl}}/assets/week8/IMG_0076.GIF)

![]({{site.baseurl}}/assets/week8/IMG_0077.GIF)

This experiment showed me how we can use our technical capacities to test or showcase a speculative circumstance. I think augmented reality, as it becomes more and more popular, will be a powerful tool for us to experiment and test designs in the real world.

&nbsp;

### INDIE and the enlightenment machine.

The 16 basic desires theory says, that there are 16 needs, desires and values that motivate a person.
The prerequisite for this theory is that all humans striving for happiness.

He had to choose one of these desires. I choose independence, the need to be distinct and self-reliant.
I built a magic machine that represented independence for me.

&nbsp;

![]({{site.baseurl}}/assets/week8/35AAA6C7-7C55-4C9E-BF3B-9E617B9512D5-1370-0000007299B1EE05.jpg)

It is a swiss army knife called *INDIE* which could be any tool you could imagine. A tool to create food, water or shelter as well as navigation, translation or any other requirement for being independent.

In the second round, I choose Curiosity, the need to gain knowledge. I created the enlightenment machine that sits on your had and tells you background information about everything you feel and do as well as the consequences of your actions.

![]({{site.baseurl}}/assets/week8/enlightmentMachine.gif)

&nbsp;

### final project.  - **did you know...**

I was working on the issue, that most of the people are not aware of the background, the process or the effect of their daily routine.

So I prototyped an on-spot information system and called it **“did you know...”**

I looked for objects in the room which are most likely available in an average household. The coffee table and the trash bin seemed to be a perfect choice. The next step was to find information about these two objects, which are not common knowledge.
Last year, I travelled through Latin America. As I was in Salento, Columbia, I realised that I didn't know anything really about coffee production.

![]({{site.baseurl}}/assets/week8/Salento.jpg)

So I was looking for a paper describing the production process and found a step by step guide.
For the trash bin, I was looking for some information about recycling and politics. What I found, was a paper from the European Union summing up the volume of trash we are producing, the recycling policies and the goals which are set for the union.

I shortened both writings to a minimum and gave it "Siri" to read. So in this way, I generated an audio file which I could perfectly link to a QR code and so provide an access point to the information a researched right on spot.

[coffee.](http://mdef.gitlab.io/gabor.mandoki/assets/coffee.mp3/) [waste.](http://mdef.gitlab.io/gabor.mandoki/assets/waste.mp3/)

I printed both QR codes and placed them at the selected spots. Now everyone could just take his smartphone, scan the code and listen to the reading while brewing coffee or throwing away the waste from lunch.

![]({{site.baseurl}}/assets/week8/APC_0629.jpg)

&nbsp;

### the conclusion.

In the beginning, I wasn't really clear about what this week was up to. In the end, I have to admit, that the exercises pointed out a lot of weaknesses in singular thought processes and that I have to work on adding more perspectives and imagination to my creative process. I'm aiming now to take a little notebook with me, and write down questions or things I am curious about as well as things I don't want to know to generate a first-person perspective on my project. Nevertheless, I want to work on a strategy to include other person perspectives as well.
