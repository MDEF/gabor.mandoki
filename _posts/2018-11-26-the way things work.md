---
layout: post
title:  "The Way Things Work."
date:   2018-11-26 19:45:31 +0530
categories: [term1]
---
![Photo by Hello I'm Nik on Unsplash]({{site.baseurl}}/assets/week7/hello-i-m-nik-403734-unsplash.jpg)

# the way things work.
---
&nbsp;

### introduction.

I was really looking forward to this week, as I have a passion for small electronics and computers. I am really convinced that understanding our devices we use every day is key for sustainable and long lasting usage, as we understand better how to handle them and also get an idea how to possibly repair them.

&nbsp;

### the new chapter on consumer electronics.

Consumer electronics, especially phones and computers, start to last longer than they used to, compared to the last decade,  when you had to update your equipment literally every year. Since the 2000s, I had to buy a new computer every 3 years and updated my phone in a 2 years cycle because of the essential technical developments over time. Last weekend I replaced my 7-year-old computer with 3-year-old second-hand MacBook and I still use my iPhone from 2015 and will keep it as long as possible - and that speaks volumes.
Of course, a new phone will be released every year, but the level of improvement is negligible for the average user.

![]({{site.baseurl}}/assets/week7/statistic_id173048_weltweiter-absatz-der-smartphone-hersteller-bis-q3-2018.png)

We see in the chart above the total smartphone shipments over the time. Since 2014 they seem not really to rise anymore. Also, the revenues growth in consumer electronics is going back from 13,2% in the year 2018 to expected a 4,5% in the year 2023 (statista.com).
So I hope that we are on top point right now of consuming new electronics like phones, computers and TVs.
But in the consequence, that means we will probably keep our products a bit longer and might want to repair them.
In the end, I have to admit, that most products released today are really bad to repair. The new Apple products, for example, got badly rated in terms of repairability on ifixit.com.

In my opinion, hardware will still be our access point to services. But I think that the importance of having the newest and best developed will fall. The Instagram app looks the same on every device. The average quality of the photo you can take with the smartphones launched over the last 3 years is absolutely high end. And you have more indicators: Only one (Apple Inc.) hardware producer is listed in the list of the [10 most valuable companies](https://en.wikipedia.org/wiki/List_of_public_corporations_by_market_capitalization). The core value of the other companies are all services or software.

Hardware will stay important as we will need it for accessibility. But services took over.

&nbsp;
### theChristmasSynth.

First, we were up to build a synthesiser with the Arduino to generate an interactive output.
The code we worked with was from Joe Marshall called "[OCTOSynth]({{site.baseurl}}/assets/week7/Octasynth.ino)".

It is a relatively complex synthesizer with not only sound generation but also with filters, analogue and digital inputs for keys, filter and volume control.

To control the synthesizer, we used another Arduino, where we connected buttons and translated their signals to simulate a keyboard.

In the end, we programmed the buttons and added some LEDs to have a "Jingle Bells" playing Christmas Synthesizer.

![x-mas Synth. photo by Vicki]({{site.baseurl}}/assets/week7/xmasSynth.jpg)

On the second day, it was absolutely clear that we had to simplify our concept. So the approach was to reduce our synthesiser to one Arduino and one breadboard. For that, I had to rethink the code and started from scratch. I used a chart I found to generate the different notes with a delay on the digital output signal.

&nbsp;

| Note    | delay in milliseconds |
| ------- | :---------------------: |
| C4   | 1911                  |
| D4   | 1703                  |
| E4   | 1517                  |
| F4   | 1432                  |
| G4   | 1276                  |


&nbsp;

And so the code was very simple. Her the example for C4:

```c++
const int speakerPin = 11;
const int buttonPin1 = 2;

int buttonState1 = 0;

void setup() {

  pinMode(speakerPin, OUTPUT);
  pinMode(buttonPin1, INPUT);
}

void loop() {

buttonState1 = digitalRead(buttonPin1);

 //C4
  if (buttonState1 == HIGH) {
        digitalWrite(speakerPin, HIGH);  
        delayMicroseconds (1911);  
        digitalWrite(speakerPin, LOW);    
        delayMicroseconds (1911);
  } else {    
    digitalWrite(speakerPin, LOW);  
  }
}
```

&nbsp;

#### So now I had to put everything together for one Arduino and one breadboard:

&nbsp;


![]({{site.baseurl}}/assets/week7/sketchSimplifiedSynth.png)

&nbsp;

#### ...and the result is:

&nbsp;

<iframe src="https://player.vimeo.com/video/302610644" width="720" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

&nbsp;

### theESPWifiSynth.

So now the next challenge was to transfer this project from the Arduino to the ESP8266 chip. Basically, this shouldn't be a problem as the ESP chip is based on the design of the Arduino. But as it turned out, the ESP2866 chip hasn't had the same sound processor as the Arduino. So I had to find another workaround.

The solve this I used the itone() function, which uses the timer to create sounds in specific frequencies.

To make the project work in the created environment, I had to add the MQTT protocol. The incoming messages in the range of 0 to 255 were remapped to 200 to 2000 as a value of the frequency (MHz).  

```c++
/*
theESPWifiSynth.
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

//=======================================================================
//       wifi.communication.
//=======================================================================

const char* ssid = "Sensors Network";
const char* password = "sensors4ever";
const char* mqtt_server = "192.168.2.227";

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("mdef/sound", "orchestra: hello");
      // ... and resubscribe
      client.subscribe("mdef/sound");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  String messageString((char*)payload);
  int messageValue = messageString.toInt();
  Serial.println(messageValue);
  int toneFreq = map(messageValue, 0, 255, 200, 2000);
  Serial.println(toneFreq);

  if (toneFreq != 0 ) {
    itone(D0, toneFreq);
  } else {
    noItone();
  }
}

#define ITONE_CYCLE 5000000  // 1 second cycle in 1/16 CPU clock (1/5 µs) for 160 MHz

//=======================================================================
//       itone, noItone
//=======================================================================

byte _itonepin = D0;
byte _itoneval = HIGH;

void ICACHE_RAM_ATTR _onItoneTimerISR() {
  _itoneval ^= 1;
  digitalWrite(_itonepin, _itoneval); //Toggle LED Pin
}
void itone(byte pin, unsigned long frequency) {
  timer1_detachInterrupt();
  timer1_attachInterrupt(_onItoneTimerISR);
  timer1_enable(TIM_DIV16, TIM_EDGE, TIM_LOOP);
  _itoneval = HIGH;
  _itonepin = pin;
  pinMode(pin, OUTPUT);
  digitalWrite(pin, HIGH);
  timer1_write(ITONE_CYCLE / frequency);
}

void noItone() {
  timer1_detachInterrupt();
}

//=======================================================================
//                               Setup
// we test the serial connection during itone running
// result: it still works
//=======================================================================

void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  delay(20);
}
```

&nbsp;

In the end, I had to change the speaker to a buzzer because the smaller wifi chip hasn't enough power to handle a regular speaker. I packed the whole setup into a phone and so every input device now had the ability to "call the phone".

We set up an MQTT server with a Rasberry Pi, so every one of us could send and receive signals.
For example, it was possible to touch a plant and change the pitch of the telephone or to open the door and to start a compressor which led to an exploding balloon.  

<iframe src="https://player.vimeo.com/video/302679136" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

&nbsp;

### the conclusion.

Sensoring, communication, internal network... all these points have been a major factor this week. Regarding my project, I can use a lot of these things. With the sensors, I could build a self-monitoring device that helps to observe yourself. The MQTT server could be a method to send you notifications about specific events via Telegram.
All in all this week provided me with a toolkit for my smart inhabitant project.  

I was really engaged by the passion of the Victor and Guillem and how easily you can make things happen.

&nbsp;

![]({{site.baseurl}}/assets/week7/phone_dissamled.jpg)

![]({{site.baseurl}}/assets/week7/phoneconnection.gif)

![]({{site.baseurl}}/assets/week7/MDEFphone.jpg)
