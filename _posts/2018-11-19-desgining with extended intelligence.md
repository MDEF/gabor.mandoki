---
layout: post
title:  "designing with extended intelligence."
date:   2018-11-19 19:45:31 +0530
categories: [term1]
---
![Photo by Franck V. on Unsplash]({{site.baseurl}}/assets/week6/franck-v-516603-unsplash.jpg)

# design with extended intelligence.
---
&nbsp;

### travelagent "Columbus".

"Columbus" was the project we did in our group to create an Artificial Intelligence.

The **goal** of this machine was to create a trip for the user to a specific place, time, budget and purpose. So the **task** for it was to collect information from defined platforms, analyse and compare this information to our profile and given framework and then to create a trip. To do so we defined the **inputs** as departure and arrival location, period, budget, purpose, personal information and access to social media data for the personal profile. As the **context of operation** we will add the community-based travelling platforms like Tripadvisor, Airbnb, booking.com or Yelp. Finally, the **output** will be a directly bookable trip, completely organised by the machine.

The method we are using for our machine is called [Collaborative filtering](https://en.wikipedia.org/wiki/Collaborative_filtering), a popular technology used by recommender systems.

![Collaborative filtering.]({{site.baseurl}}/assets/week6/Collaborative_filtering.gif)

*This image shows an example of predicting of the user's rating using collaborative filtering. At first, people rate different items (like videos, images, games). After that, the system is making predictions about user's rating for an item, which the user hasn't rated yet. These predictions are built upon the existing ratings of other users, who have similar ratings with the active user. For instance, in our case the system has made a prediction, that the active user won't like the video.* - wikipedia.org

The next thing we discussed was what kind of **Machine Learning** will our artificial brain use?

![copyright by edureka!](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2018/03/How-Machine-Learning-Works-What-is-Machine-Learning-Edureka-1.gif)

As it has access to a huge database, we decided to only give the machine a matrix and so it should learn **unsupervised** to find the patterns and clusters by its own. For clustering, we would use the **k means** method that divides each observation into clusters and assigns them to the cluster with the closest match. These clusters were used to build the recommendations.

![Voronoi Cells](https://upload.wikimedia.org/wikipedia/commons/thumb/5/54/Euclidean_Voronoi_diagram.svg/512px-Euclidean_Voronoi_diagram.svg.png)

So, in general, our machine was running now. So we discussed if we should or how we could implement neural networks. For its core purpose, it is not needed but we developed the "TimeMachine" feature. We are using a neural network to predict a future trip for us we didn't know about.

&nbsp;

![]({{site.baseurl}}/assets/week6/PNG-Bild.png)

However, creating such a machine provokes to discuss the ethical aspects as well as about the issues of this machine.

*"Columbus"* has to deal with a lot of very private and very sensible data. So we focused first on safety.
The user can decide in the first place what data he wants to release. All data used, will be encrypted and anonymised.

The other two points, fairness and discrimination, got our focus. We discussed the main issues. One is that we can’t guarantee access to everyone. Internet Connection and participation in social and data mining platforms is necessary for usage. We also have to deal with the possibility that the machine could focus on places which are already overcrowded as Barcelona for example because it has the most attendance in social communities. There could be blind spots generated this way.

&nbsp;

### conclusion.

This week about artificial intelligence didn’t give us only an insight of its powers and capabilities but also showed its weaknesses. The discussion about the humanitarian biases implemented in the machines made me think about how to solve this.
During another lecture I was about the diversity in AI, the presenter said that the language is the biggest problem regarding bias. So is it possible to create languages for different purposes without any vocabulary that makes being biased impossible? An example would be a language for recruitment machines without gender and ethnicity.
Besides the fact that we should hardly work on our self-being as less bias as possible, we can create tools to promote this for the next period of technology.
Can we clean a database for being racist for example by changing the vocabulary?

The other question is, can we create an intelligence we train with all moral and ethical codexes we know, to have it as an assistant for questions regarding society and politics? Can it be so superior one day by training itself that we can leave all discussions about killing the pedestrians or the passenger behind us?

All in all, this week revived my interest in AI. Right now I don’t know how, but I will work on to implement principles and parts of this technology into my final project.

&nbsp;

------

#### links:

[wekinator.org - Developed for artist and musicians to create their own machines with machine learning.](http://www.wekinator.org)

[MINIST database.](https://en.wikipedia.org/wiki/MNIST_database)

[LSTM](https://en.wikipedia.org/wiki/Long_short-term_memory)

[CNN - technology mostly use for visual analytic.](https://en.wikipedia.org/wiki/Convolutional_neural_network)

[GANs - visual maipulation method.](https://en.wikipedia.org/wiki/Generative_adversarial_network)
