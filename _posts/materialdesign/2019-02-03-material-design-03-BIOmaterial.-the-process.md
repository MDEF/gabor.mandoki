---
layout: post
title:  "material design 03: BIOmaterial. the process"
date:   2019-02-03 10:00:00 +0530
categories: materialdesign term2






---

![]({{site.baseurl}}/assets/term2/materialdesign/finalround/IMG_4669 2.jpg)

# spent grain. the process.

------

For more information about the material, go to the [previous post]({{site.baseurl}}/materialdesign/term2/2019/01/31/material-design-02-BIOmaterial/).

&nbsp;

## the procurement process.

The first batch of the material I presented was straight from our neighbourhood, the brewery called "Edge" in Poble Nou.

<iframe width="900" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=2.1913754940032963%2C41.39196222372321%2C2.1951735019683842%2C41.3962158827233&amp;layer=hot&amp;marker=41.39408707587948%2C2.19327449798584" style="border: 1px solid black"></iframe><br/><small>

The people working there were really nice and supportive. So after presenting the material, Oli and I went back to them to get more of the grain. We talked a bit with the brewmaster about the process and that they give the malt to a local farmer.

The material itself is mainly barley. This is the most common grain. Depending on the sort of beer they are brewing, it can change. It lost all its sugar after the brewing process.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/documentation/IMG_4481.jpg)

&nbsp;

After getting back to the university, with four sacks full of malt, we put the majority of it on the roof to dry directly in the sun.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/documentation/AAFFF1FB-7CDD-440B-A6C8-68E843AE0F66-4791-000001F25F30D21B.jpg)

&nbsp;

Everyone who was up to working with this material was free to use from this batch. I took a bit of it home with me to dry it directly in the oven because I wasn't sure how fast it can dry only in the sun as we are in February. In the end, it turned out it took a while and wasn't very efficient.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/documentation/EAEC0BCE-B46F-4E0C-8F2A-77ACF4130EEC-4791-000001F27A22C7D2.jpg)

&nbsp;

## first lap: drying and shredding the material.

The first thing I wanted to do, was to dry the material. It already started to smell really bad after one day in a closed can. I lied it out on a baking paper sheet and dried it very slow with a slightly open oven by 50 degrees. It took around a day to dry it properly. (I don't want to see the electric bill)

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/theprocess/oven_inside.jpg)

&nbsp;

1. took spent grain freshly from the brewery - was still warm
2. put it in a can, kept in my locker overnight
3. next day,  started to smell a bit sour
4. took it home and dried it in the oven at 50 degrees with an open door - took the whole evening and to the next half a day to dry.
5. result: reduced the sour smell and is dry

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/theprocess/raw_dried.jpg)

&nbsp;

After having a good amount of dried material, I wanted to grind it as good as possible. I tried a wooden mortar first, what didn't work that well. So I used an electric blender and shredded the dry material as small as I could. After that, I sift the material additionally what made a nice powder mixed with a bit of fibre. That resulted in three different conditions:

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/documentation/21C2E8AE-236E-4653-8A6B-0EFCA7BE4CEF-1048-000000774CA708FB.jpg)

&nbsp;

1. grind the material in the blender
2. sift the material to get rid of all rough parts
3. see if the powder is a good basis for further experiments

&nbsp;

After blending I sifted the material, added a bit of water and started to boil it. I only added a slight bit of water and added some more any time at vaporized. It started to became a nice paste but wasn't really sticky. After 20 minutes of boiling, I tried to form a ball. I added a bit of rougher grain so that it became more sticky.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/documentation/60DFFC1E-02CE-40D0-A1E6-99B1F3752339-1094-000000874EAF1291.jpg)

&nbsp;

For the second run, I blended the grain together with water and boiled it then. This was much smoother. I formed a brick and made a thin layer.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/documentation/A6C12118-CBED-4E23-81FA-69BE038CFF78-1094-0000008DEF6D7F35.jpg)

&nbsp;

I put all three objects in the oven to dry them out. I was really happy with the result as it only took the material itself, some water and heat to create some shapes. But before sticking to that method, I wanted to experiment with some different methods to see what path I would like to go.

&nbsp;

------

## next step: using yeast instead of water.

In the meantime, a few other people joined the club of beer malt. And so Julia organized a visit at another brewery called [Birra08.](https://birra08.com)

&nbsp;

<iframe width="900" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=2.1903093159198765%2C41.40651787990836%2C2.1913285553455357%2C41.40758108260541&amp;layer=hot&amp;marker=41.407049986365735%2C2.1908189356327057" style="border: 1px solid black"></iframe><br/><small>

&nbsp;

We had a nice tour through the small brewery where we were led through the whole brewing process.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/secondround/breweryvisit/DSC_0567 (1).jpg)

![]({{site.baseurl}}/assets/term2/materialdesign/secondround/breweryvisit/DSC_0571 (1).jpg)

&nbsp;

In addition to the other brewery, we also received some yeast.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/secondround/breweryvisit/DSC_0572 (1).jpg)

&nbsp;

After the success of the first experiment, I wanted to do another test. But this time, I used yeast from the brewery instead of water.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/secondround/IMG_4615.jpg)

&nbsp;

I ground the dried malt to get a really nice a fine flour.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/secondround/IMG_4616.jpg)

&nbsp;

That was the best result I could achieve. But it was fine for the next steps. I took a bit of the material and added some yeast from the brewery. I added bit by bit to keep control of the consistency of the mix. In the end, I formed to bodies: one ball and a flat round shape.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/secondround/IMG_4618.jpg)

&nbsp;

I baked them overnight. While the flat surface was nice and fine, the ball cracked. So my conclusion is that I go on working with water as yeast didn't work out for me to be a good binder as I expected.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/secondround/3EB3C8B6-198E-47FE-B641-6569CA6BEBA9-10743-00000902A4A523F5.jpg)

&nbsp;

------

## next step: bioplastic.

After the lap with the yeast, I was up to do some experiments with bioplastic. For this purpose,  I worked together with Ola and Ryota. Gabriela helped me out with some raw material and the basic recipe.

&nbsp;

- [x] 6g agar agar
- [x] 6g glicerol
- [x] 15 ml white vinager (antibacterial)
- [x] 200 mL water to cook in medium temperature.
- [x] 80g of grinded malt
- [x] Cook for about 10min - check texture.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/plasticround/IMG_4624.jpg)

&nbsp;

Besides this recipe, we did a few more.  However, for me, that worked the best. It took a bit longer than 10 minutes and we had to add a bit more water during the process.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/plasticround/IMG_4631.jpg)

&nbsp;

As it started to feel a bit sticky and plasma-like, we put the mass into a mould Ola printed the day before and let it dry overnight. The result was a little bowl.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/plasticround/IMG_4678.jpg)

![]({{site.baseurl}}/assets/term2/materialdesign/plasticround/IMG_4679.jpg)

&nbsp;

As much as I enjoyed creating some bioplastic, I didn't like the idea to at some much external material.

&nbsp;



------

## final lap: back to basics.

After all the experiments, I still wanted to make something only out of the material.

So, my plan was to create a cup to see if I can make a 3D object. I started to blend the dried material together with water. I gave it a good blend so that it was as smooth as possible. The amount of water was only the minimum needed to blend it properly.

I cooked the mass for 20 minutes at medium heat. After this time, I wasn't happy about the consistency. So I added a bit more water and boiled it for another 20 minutes. Now it reached a really nice homogeneous structure. I prepared a cup by wetting some baking paper and putting it in. Additionally, I wrapped a smaller glass also in the baking paper. Having this prepared, I filled the mass into the cup and pushed the smaller glass in it so it became a nice mould.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/finalround/IMG_4655.jpg)

![]({{site.baseurl}}/assets/term2/materialdesign/finalround/IMG_4656.jpg)

![]({{site.baseurl}}/assets/term2/materialdesign/finalround/IMG_4657.jpg)

&nbsp;

I put it in the oven at 50° for half a day to give it a nice and slow dry.

After a half day, I took out the smaller glass so I could dry the cup better from the inside. After an additional day of drying in the oven and letting it cool down, I could take the result out of the cup as well.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/finalround/IMG_4660.jpg)

![]({{site.baseurl}}/assets/term2/materialdesign/finalround/IMG_4661.jpg)

&nbsp;

So the last step was to get rid of the baking paper. I was really careful as the whole structure is really fragile. The result is a really nice arty looking cup, with holes, (most probably not waterproof) but only the material itself.

&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/finalround/IMG_4667.jpg)

![]({{site.baseurl}}/assets/term2/materialdesign/finalround/IMG_4669.jpg)
