---
layout: post
title:  "exploring hybrid profiles."
date:   2018-10-29 19:45:31 +0530
categories: [term1]
---
![by Gabor Mandoki]({{site.baseurl}}/assets/week4/IMG_1307.jpg)

# exploring hybrid profiles.
---
&nbsp;

### day I:

**Meeting with Ariel Guersenzvaigl @ ELISAVA**

First of all, what I really enjoyed, was discussing with Ariel. He has a deep understanding of human, machine, and design in relation to each other.

His background, coming from the DOTCOM period, I think equipped him with some of the core techniques of our today's (digital)economy. Most of the things, that we use daily, started with or right after this time.

**Web Designer** *to* **Information Architect** *to* **Interaction Designer** *to* **what he is today...**

I didn’t know the terminology of Information Architect, but it seems to be a really straightforward way of seeing things. Building with data. *(Something all our services use? Or is this a different thing? read: [Richard Saul Wurman: Information Architects](https://books.google.es/books/about/Information_Architects.html?id=ivRCPgAACAAJ&source=kp_book_description&redir_esc=y))* & [The Polar Bear Book](https://intertwingled.org/the-polar-bear-book/)

***Science vs. design methodology.***

*Very inspiring:* The way he described design process. [Sketches of Frank Gehry](https://www.youtube.com/watch?v=vYt2SQPqTh0)

*"By solving a problem, you understand the problem."*  - contrary to the scientific methodology.

It was really interesting to see how social and econmical events like the DOTCOM crash influenced his career.
Also how he gives ethics and moral a huge space in all his thinkings and design models. I can completely acquire a taste of this importance.  

**books to read:**

[Developing Citizen Designer by Elizabeth Resnick](https://books.google.es/books/about/Developing_Citizen_Designers.html?id=gmp8rgEACAAJ&source=kp_cover&redir_esc=y)

[The primary generator and the design process by Jane Darke](https://www.sciencedirect.com/science/article/pii/0142694X79900279)

[How designers think by Bryan R Lawson](https://www.researchgate.net/publication/30872105_How_Designers_Think_-_The_Design_Process_Demystified)

[Designerly Ways of Knowing by Nigel Cross](https://link.springer.com/chapter/10.1007%2F1-84628-301-9_1)

[Understanding Design by Kees Dorst](https://www.goodreads.com/book/show/487723.Understanding_Design)

**links:**

[Computing in the Humanities at University Bamberg](https://www.uni-bamberg.de/en/ma-cith/)

[Moral Machine MIT](http://moralmachine.mit.edu)

["AlphaGo" at Netflix](https://www.netflix.com/es-en/title/80190844)

---
&nbsp;

### day II:

**Meeting with Sara Derieta @ her workshop**

I liked the way she took a really old profession as shoemaking and redeveloped it with her own concept. She put more design philosophy in it than it probably used to have.

**Architecture** *to* **master craftswoman in Shoemaking**

Motivated by finding working as an architect too abstract, she had the desire to do more hands-on design. Her way could be the perfect template for a lot of designers with the same lack of manual work. In my opinion, this particular part of designing became a niche sector in the mainstream by only using digital tools and interfaces.  

I really liked her approach to find new materials and reinventing them. As a vegan, she is obviously not into working with material from dead animals. Therefore, she tried to find other partly really abstract materials for creating new shoes.  

![Mushroom shoe]({{site.baseurl}}/assets/week4/Image_05.11-2.jpg)

So what it says to me, even if the profession or the process is well worked out… take it, improve it and make it yours.

![Shoemaking]({{site.baseurl}}/assets/week4/APC_0614.jpg)
![ShoemakingII]({{site.baseurl}}/assets/week4/APC_0617.jpg)

---
&nbsp;

### day III:

**Meeting with [Domestic Data Streamers](https://domesticstreamers.com) @ [their office](https://www.google.com/maps?ll=41.397117,2.194006&z=15&t=m&hl=de-DE&gl=ES&mapclient=embed&q=Carrer+de+Pujades,+77+08005+Barcelona)**

The moment arriving in their office, you are accessing a great atmosphere of creativity, open mind and communication. I really enjoyed how the guys represented themselves.

The UNICEF project was remarkable. The way the task was turned into equating all participants in terms of their childhood memories showed their capability of thinking outside of the box.

![Sitting at Data Center]({{site.baseurl}}/assets/week4/Image_05.11.jpg)

Design Does was not only a great project for enlighting people how you can see design and the impact of design, but also an inspiration how t ono reevaluate the consequences of your design.

![pocket data]({{site.baseurl}}/assets/week4/IMG_0032.jpg)

These skills of communication and outside the box thinking, combined with a great workspace and atmosphere serves me as a template for my future work environment.

![overview achivement]({{site.baseurl}}/assets/week4/Whiteboard for exploring hybrid profiles achievement.png)

### Knowledge & Experience.

#### inspiration.

Ariel Guersenzvaig inspired me the most regarding knowledge and experience.

His expertise in different fields that he gained over the time with the combination of professional experiences and the academic path pretty much represent the concept I seek for. Coming from web design, over information architecture to interaction design and philosophy shows me how he evolved himself more and more to work more human-oriented. Implementing philosophy and so ethical and moral principles in the designer world seems to be a desirable model of design thinking.

#### where I am from...

I already tried to learn and work in different environments. But there is a lot more to come.

Before I finished high school, I worked as a composer for a multimedia communication company and collected experiences in working for different clients in different countries.

After that, I decided not to go on with this profession but still stay in the creativity business. Studying communication design opened up a completely new world for me. During my studies and the time after, I worked as project and event manager for a company in the entertainment industry. It was a great experience as well but after a while, I was sought for a change and another purpose.

*Now I stranded here in this master and want to learn all the tools I need to shape society by eliminating injustices and create a new understanding of living together with each other.*

#### ...where I go to.

Over time, I would like to fall back on a big catalogue of knowledge in the fields of politics, society, technology, history, psychology and philosophy to create the capability understanding "why things happen".

In addition, I would like to live and work in different countries of the world. Right now, I am thinking about China, Japan, the US, New Zealand and different countries in Europe.

I would also like to continue working in different professional fields to learn about different perspectives and strategies. I could also imagine doing another master some day or give teaching a try.

&nbsp;

### Know-how & Skillset.

#### inspiration.

I have a huge respect for craftsmanship, especially in traditional fields as shoemaking, carpenter or binder.
That's why Sara inspired me the most regarding know-how and skills and how she took a long-established craft and redesigned it to make it her own.

#### where I am from...

Right now, I have different skill sets but mainly in digital creation. I have skills in creating music, graphics, and photos for example but all mainly digital. The only hands-on design situations I have is doing music with my guitar or analogue synthesizer, shooting with my analogue camera or create print media.

#### ...where I go to.

I would like to enhance my hand on-design skills like drawing, develop analogue photos and get more used to work with raw materials like wood, metal or even biomaterial.


&nbsp;

### Attitude & Mindset.

#### inspiration.

In terms of attitude, all three days inspired me in different ways.

Ariels approach combining design with humanity is really important to me,

Sara's approach redefining a long-term established profession is very inspiring and

Domestic Data Streamer's way of thinking outside of the box to achieve their communication mission is a goal to achieve.

#### where I am from...

Today I have in mind to enrich people with my design in terms of knowledge exchange, education as well as entertainment. I am still working on planning my process to achieve a more unconventional way to do so.

#### ...where I go to.

Improving my knowledge about psychology and philosophy will focus my work on human interaction as well as challenging established methods to find new ways. I will also put more effort to make a good first impression as this is a core element to enhance the short-term impact of your work.

&nbsp;

### conclusion.

My future me as a designer should focus on human inspired design giving ethics and moral a significant source of framing my work.

I would like to implement this way of thinking into public organizations or politics to promote sanity in decision making.

My skills will be enhanced by learning new design methods and fabrication as well as improving my way of communication. I will focus more on hands-on design as well, to get a better idea of how to create a haptic direct feedback in design and improve interaction.

I want to extend my knowledge in psychology, politics, and philosophy to understand "why things do happen" as well as in technology and design methodology.

&nbsp;

### vision.

One idea of mine is to implement or create a system or platform wising people about their habits (skills, knowledge etc), how they can be improved or what impact it has and to give a tool which shows realty over "fake".

For me, it is a huge problem that our society today has tools which are more popular by spreading fake news than tools which are educative or fact-based.

**why?**

Because I think it is time to use the tools we got to trigger a new age of enlightenment instead of stultification.

**how?**

I think transparency is the key for that. Blockchain for example can be a tool to generate transparency in politics, production or society without harming privacy.

**what?**

A platform accessible for everyone filled with knowledge, facts, and tools* to improve.
I want to generated a set of tools promoting this idea Or following the principles transparency.

 **[Smart citizen](https://smartcitizen.me) is for me a perfect example of how a tool of this platform could look like.*  

[Simon Sinek "Start With Why"](https://www.ted.com/talks/simon_sinek_how_great_leaders_inspire_action?language=en)



***first term***.

Navigating the uncertainty, exploring hybrid profiles had the greatest impact on my goal, reflecting on how I want to be and become. I pretty sure there is a lot more to come with being your own system and introduction to futures.

Biology zero, design for the real digital, the way things work and from bits to atoms are the programs adding tools for my hands-on approach.

I am missing some methods of project management and project development.



***links.***

[Design for the unknown](https://www.researchgate.net/publication/254941682_Designing_for_the_unknown_a_design_process_for_the_future_generation_of_highly_interactive_systems_and_products)

[Annoted portfolios and other forms of intermediate level knowledge](http://interactions.acm.org/archive/view/january-february-2013/annotated-portfolios-and-other-forms-of-intermediate-level-knowledge)

[Designing for, with or within](https://www.semanticscholar.org/paper/Designing-for%2C-with-or-within%3A-1st%2C-2nd-and-3rd-of-Tomico-Winthagen/cfec7ddb0861f0b5ad32bf87f252acff8a16b2e0)

[Cultures of resilence](http://culturesofresilience.org/wp-content/uploads/resources/CoR-Booklet-forHomePrinting.pdf)
