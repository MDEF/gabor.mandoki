---
layout: post
title:  "updated: fab 09: embedded programming."
date:   2019-03-21 10:00:00 +0530
categories: fabacademy


---

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week9/screenshots/Screen Shot 2019-03-17 at 14.37.09.png)

# Embedded Programming.

------

&nbsp;

I already programmed my two boards successfully in the previous weeks.

[Electronic Production.]({{site.baseurl}}/fabacademy/2019/02/26/fab-05-electronic-production.-updated/)

[Hello World Board.]({{site.baseurl}}/fabacademy/2019/03/07/electronic-design/)

## Setting up Platform IO.

So I challenged myself for this week to switch from Arduino IO to Plattform IO and to program an SOS blinking with Python. I watched this video as a guideline to set everything up inside Atom.

<iframe width="740" height="415" src="https://www.youtube.com/embed/EIkGTwLOD7o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

It guided me through the essential steps and to understand how this would work.

Everything set up, I tried to import my Arduino code and settings from the week before. This made my hello echo board only blink when I pushed the button. I understood the file structure inside the platform IO and started to debug the code and the settings.

Here is some advice:

- the file called main.cpp is a C file that you can compare to the code file in Arduino. Everything you would like to programme has to be written in here.

- the plattform.ini file is the configutraion file where you can set the board, framework, upload port and so on. here are some things i used:

```python
[env:attiny44]
platform = atmelavr
lib_extra_dirs = ~/Documents/Arduino/libraries
framework = arduino
board = attiny44
; change microcontroller
board_build.mcu = attiny44
; change MCU frequency
board_build.f_cpu = 20000000L
; upload_port & programmer
; upload_protocol = usbtiny
; upload_port = /dev/cu.usbserial-FT9P085Z
```

You can find some of the values like the MCU frequency in the [datasheet.](http://academy.cba.mit.edu/classes/embedded_programming/doc8183.pdf)

Here are some sources from the platfrom IO docs:

upload port:

[Upload options - PlatformIO 4.0.0a5 documentation](http://docs.platformio.org/en/latest/projectconf/section_env_upload.html)

configuration for programmer:

[Atmel AVR - PlatformIO 4.0.0a5 documentation](http://docs.platformio.org/en/latest/platforms/atmelavr.html#upload-using-programmer)

configuration for attiny44:

[Generic ATtiny44 - PlatformIO 4.0.0a5 documentation](http://docs.platformio.org/en/latest/boards/atmelavr/attiny44.html)

board options:

[Board options - PlatformIO 4.0.0a5 documentation](https://docs.platformio.org/en/latest/projectconf/section_env_board.html)

It's not working yet... I will continue as soon as I got through with troubleshooting.
I would also like to work a bit with microPython.

### SOS code.

This is the code to let the LED blink "SOS".

```c
#include <Arduino.h>

// const int buttonPin = 3;     // the number of the pushbutton pin
// const int ledPin =  7;      // the number of the LED pin

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(6, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(6, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(6, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

### Datasheet.

The datasheet, in general, is your starting point when you design a board. You can get the power, how the pins are arranged if it can do the job you want to have it for etc.

So I tried to find if I can use an ATTiny44 for MicroPython. So I looked for the minimum requirements for [MicroPython](https://github.com/micropython/micropython/wiki/FAQ) and checked in the sheet how much memory the ATTIny has.

|              | ATTIny 44 | microPython |
| :----------- | --------- | ----------- |
| Flash Memory | 4k        | 128k        |
| RAM          | 256BYTES  | 8KB         |

So the ATTiny is too small for micro Python. I have to try with another microcontroller. 
