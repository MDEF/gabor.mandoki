---
layout: post
title:  "updated: fab 02: setting up GitLab."
date:   2019-01-30 10:00:00 +0530
categories: fabacademy



---

![]({{site.baseurl}}/assets/fabacademy/week2/pankaj-patel-729895-unsplash.jpg)

# GitLab.

------

## setting up the page.

GitLab is the platform we are using to host our homepages and also document our whole process.

As we already set up this page at the beginning of the master we just had to add a FabAcademy section to our already existing page.

I just copied the structure of my index.html page, modified it a bit so that the FabAcademy section on my page is only showing the FabAcademy related post. You will still find all posts including my posts from MDEF and FabAcademy on my landing page.

The preset I used originally is called [Gravity](http://jekyllthemes.org/themes/gravity/) and is based on Jekyll.
I am using a duel system of Atom and Hyper to manage the site. I tried different editors, but I like the interface and the Git integration of Atom the most. *update:* with the installation of Platform I/O on top of Atom, a terminal is now integrated into Atom. I still use Hyper sometimes but it became really unlikely because of that. 

### recommended software:

[Hyper](https://hyper.is)

[Typora](https://typora.io)

[Atom](https://atom.io)

[Platfrom I/O](https://platformio.org) (tool for programming hardware but has a bunch of nice tools for Atom)

Didn't try it but looks quite solid especially for documentation like Fabacademy: [GitBook](https://www.gitbook.com)

## creating a website on GitLab.

- create a GitLab account
- create a new project
- and follow this [doc](https://about.gitlab.com/product/pages/) which explains to the principal how to set it up
- additionally, and I would recommend, read this [readme file](https://gitlab.com/pages/jekyll/blob/master/README.md) how to set up a Jekyll page on GitLab - that's how I did it.

alternative: watch this youtube tutorial. It guides you through the whole process.

<iframe width="740" height="445" src="https://www.youtube-nocookie.com/embed/TWqh9MtT4Bg?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## managing the filesystem.

To get an idea of how I manage my files, check out [my repo.](https://gitlab.com/MDEF/gabor.mandoki)

I have a clone of the repo on my computer. I work on the clone first to generate posts or pages. To each post, project or page, I have an additional folder in assets which contains all media and project files. 

To publish the files and the post, you have to push them to your GitLab repo. Here is a [basic guide](http://rogerdudler.github.io/git-guide/) how git works with commands. Atom is the tool to use if you don't have a basic understanding of command line tools.

I always link the files to my page so that everyone can directly download it from the page and the repo.

example screenshot:

![]({{site.baseurl}}/assets/fabacademy/week2/Screen Shot 2019-04-20 at 13.09.12.png)

### step-by-step.

After creating a new post and adding stuff to your folder structure, you can test your site before pushing it to GitLab (if you are using Jekyll as your static site manager). 

For that do the following:

Oen your terminal app.

type cd and drag your repo folder into the terminal window. Press enter. Now you are inside your project folder.

Now, type "jekyll serve". It will take a few seconds to build a local version of your page. Then a link pops up. Click on it and your browser will open and show a locally hosted version of your page. You can still edit your files, save them and than refresh your browser and see the changes. This workaround makes your life way easier to test your posts and files. 

If you like my design, I am more than happy to share the modified theme. Contact me via email and Ill help you to set it up.