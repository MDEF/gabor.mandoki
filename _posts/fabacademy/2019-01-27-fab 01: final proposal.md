---
layout: post
title:  "fab 01: final project proposals."
date:   2019-01-27 10:00:00 +0530
categories: fabacademy

---

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week1/jan-antonin-kolar-1193333-unsplash.jpg)

# FabAcademy project proposals.
---
## SmartSpeaker for the NightStand.

My proposal focuses on a device that is part of the infrastructure I am up to build with the ["Emanuel"](https://mdef.gitlab.io/gabor.mandoki/2018/12/26/design-dialogues/) project. The Idea is to build an alarm and music box for the nightstand. This music box should provide the following features:

### for the morning:

- AlarmClock with randomized alarm sounds. The source is a predefined playlist.
- another source could be a preselected radio station as well as specific news podcast.
- OLED screen that only shows the time.
- snooze.

### for the evening:

- a selection of audiobooks and podcast are stored.
- on request, one of the audiobooks or podcasts is played.
- additionally, you can also listen to soundscapes like rain, wind or waves.

### additional features:

- a voice interaction (based on snips.ai).
- gesture control.
- integration of the "Emanuel" information infrastructure.

### required hardware:

- wooden box.
- OLED screens.
- knobs & buttons.
- Raspberry Pi with alReSpeaker Mic for Voice Control.
- Arduino for controlling inputs.
- Ultrasonic sensors or 3D gesture pad for gesture control

### required software:

- AlarmClock
- music and audio file player
- snips.ai



When this project is done, I would like to add a companion. The idea is to have an ePaper you can hang anywhere you want to receive information, kitchen for example. When you are not using it, it shows a picture or whatever you prefer.

Else, it can show recipes, the daily news while you are drinking your coffee or any historical or current event in your neighbourhood. It can also provide information about your the actual analyses of your daily habits provided by Emanuel.

The interaction can be in touch with the help of IR LEDs and CIS sensors, gesture with ultrasonic sensors and voice with the snips.ai as a basis.

---





## old proposals.

## 1. multi input interaction hub.

With my project in the FabAcademy, I want to test different options for interaction. I want to build a hub that reacts to voice, gesture and touch. So, I decided the project into 3 parts:

### touch surface.

For the touch surface, I would like to build a frame with IR LEDs and CIS sensors you can place on any flat surface and create a "touchscreen". The surface is multi-touch capable and scalable.

Requirements:

- IR LEDs
- CIS sensors
- Arduiono or ESP32
- frame

![]({{site.baseurl}}/assets/fabacademy/week1/5982821508367568749.jpg)

### 3D gesture control.

The second component is a 3D gesture control unit. With to ultrasonic sensor, I would like to measure the positions of the hands of the user and control the machine.

Requirements:

- 2 ultrasonic sensors
- Arduino or similar

### voice interface.

The voice interface is based on my prototype from term 1: Raspberry Pi, Seeed Mic and one of the open source voice recognition systems of Mycroft or Snips.

Requirements:

- Raspberry Pi
- Seeed Pi Hat Mic
- Speaker

### Hub.

In the end, I want to put this three inputs into a hub like a big screen table or just a frame which helps you to make any surface to a multi-input interface with the help of a projector.

I also need to define an operating system for the system where the input will process the inputs.

Most probably I will use a Raspberry Pi and Rasbian as the core system and a simple game like Pong as test field.

![]({{site.baseurl}}/assets/fabacademy/week1/eink_featured.png)

## 2. ePaper information tablet

An ePaper inforamtion tablet good be the information access interface. A tablet running Android fE that is lying next to your bad instead of your phone providing you news, information about your invioment (city, apartment etc.) and healthdata without the abstract of incoming notifications of your phone.

Requirements:

- Raspberry Pi
- eInk display / touchscreen
- information infrastructure / OS

## 3. MultiSensor Hub static / dynamic

For my infoamtion and awearness system, it is required that I observe the habits and the chnages of the direct invorment. An example for that are the super seonsors:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/aqbKrrru2co" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Requirements:

- multiple sensors
- multiple microprocessors
- backend

The porblem with this project is that is mostly reqired for my final project but I am not sure if I am able to do this in this short amount of time.

Eather way, I want to follow this idea as well to evaluate the possibilties.
