---
layout: post
title:  "fab 04: computer controlled cutting."
date:   2019-02-13 15:00:00 +0530
categories: fabacademy


---

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/fotos/IMG_4594.jpg)

# computer controlled cutting.



## laser cutting.
---


![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/renderings/pressfit_rendering_3.png)
### design press-fit model.

For the press-fit kit, I made a parametric design in Fusion360. I designed a triangle and a joint with 30 degrees.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/screenshots/FixJointSketchTri.png)

As you see, I decided to create an isosceles triangle with the side length of 50mm. I only created one cut out and mirrored it to all positions.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/screenshots/FixJointSketchJoint.png)

The joint was designed to have an angle of 30 degrees in total, so 90 - 15 on each side. I had to remember the ties before my high school degree for the basics of geometry as it was the last time I used it :)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/screenshots/FixJointParameters.png)

Here you can see all the parameters I added to the design.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/screenshots/FixJointJoint.png)

To see how my values are working I built a little model inside of Fusion. And then I rendered it:

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/renderings/pressfit_rendering_3.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/renderings/pressfit_rendering_2.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/renderings/pressfit_renderin.png)

I planned to cut it in cardboard but decided on the fly to cut something else instead...

### notebook stand.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/fotos/57175068217__224BBB81-4066-4C06-A0F8-65B681F78BB6.jpg)

I designed a notebook stand for my MacBook 15". You can download the file [here]({{site.baseurl}}/assets/fabacademy/week4/files/notebook stand lasercut files.3dm) and change the parameters for your own purpose.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/screenshots/NoteBookStand3D.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/screenshots/NoteBookStendParameters.png)

Here you can see what parameters I added. I had to add some variables. I also reduced to the value of the notebook width with 4cm in total to make sure that the MacBook is held securlt. I would recommend that for your values as well.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/screenshots/NoteBookStandCutFile.png)

After designing the model, I exported the design as dxf files and imported them to Rhino to prepare the cut file.
I added a graphic of melting vinyl I created a couple of years ago to add some engraving. To also have some rastering, I added my name as a raster graphic.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/fotos/IMG_4600.jpg)

We set up the file on the computer for the laser cutter. I collided with Oliver. We cuttet two designs on a piece of plywood 60cmx60cm with the thickness of 4.1mm.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/fotos/IMG_4597.jpg)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/fotos/IMG_4599.jpg)

We were running through all the settings of the printing assistant and finally started with the rastering and send the engrave and cut job immediately afterwards.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/fotos/IMG_4594.jpg)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/fotos/IMG_4596.jpg)


We had a bit of an issue because the material wasn't completely flat. So the material wasn't cut through on every line. We used the cutter to release the parts. Then I glued all the parts of the notebook stand. That made me realise that I have to recapture the parameters of the kerf of the cutter as some jints were a bit to lose and others were not deep enough. Probably it had also to do with the not flat material. But everything worked out in the end and this is the result:

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/fotos/BC526D4F-551F-4686-911C-FD5CAE7FC414-10935-00000A522BFA6655.jpg)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/fotos/C688162B-B776-4304-9E32-49D95DD6D253-10935-00000A521873225D.jpg)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/fotos/IMG_4609.jpg)



### group submission.

The group submission was a bit of a failure. We tried to cut this joint test file:

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/screenshots/LaserGroupAssigment.png)

... and the result was this:

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/fotos/IMG_4591.jpg)

But with this failure, we learned a lot. We had the wrong settings and the wrong material. The cardboard was 7mm thick which made it really difficult to cut properly. It wasn't cut through but the material already burned. We talked to the other groups, analyzed their results and gathered their experiences. And then we collided in smaller groups to put the designs and files together and cut. It worked out pretty well.

### step-by-step.

1. export files to rhino.
2. put it into a rectangle the size of the printer
3. layers:
   1. red: engrave
   2. blue: cut
   3. black: raster
4. send to the printer  - change properties and view that it fits.
5. make a test cut
6. simplify the cutting job
7. cut

## vinyl cutting.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/fotos/IMG_4605.jpg)

I took the illustrator file from the graphic on the notebook stand and reduced it to some outlines.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/screenshots/VinylStickerIll.png)

I made it perfectly fit the Apple logo on my MacBook, that to vinyl could shine.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week4/fotos/IMG_4603.jpg)

I used the small laser cutter in the lab and put it on my MacBook. I had to do it twice because the small point in the middle was too small and I stuck it to the wrong position first. You can download the design [here]({{site.baseurl}}/assets/fabacademy/week4/files/LiquidVinyl sticker one layer.dxf).
