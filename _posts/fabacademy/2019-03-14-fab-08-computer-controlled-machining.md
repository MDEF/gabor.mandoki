---
layout: post
title:  "updated: fab 08: computer controlled machining."
date:   2019-03-14 10:00:00 +0530
categories: fabacademy

---

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week8/photos/IMG_4972.jpg)

[Download Fusion360 file.]({{site.baseurl}}/assets/fabacademy/week8/files/Viking Chair cleaned up v6.f3d)

# Computer controlled machining.

------

&nbsp;

## Design something big.

For this week, we were supposed to build something big. So, I decided, not that big but useful, to create some furniture for my balcony. I wanted to keep it simple and I decided to create Viking Chair or Plank Chair like [these](https://pin.it/aft5k6h3owfyft), and simple table.

I measured my balcony and started to design in Fusion. You will find my documentation about working with Fusion [here](https://mdef.gitlab.io/gabor.mandoki/fabacademy/2019/02/06/fab-03-CAD-3D-model/).

Here are some things you have to take care of while designing these components:

- Be sure to have the thickness of your material as a parameter and design with it to keep it easily parametric.
- Try to build the parts together visually to see if your design makes sense. You could also cut a smaller model on the laser cutter. For my guide for the laser cutter follow this [link](https://mdef.gitlab.io/gabor.mandoki/fabacademy/2019/02/13/fab-04-computer-controlled-cutting/).
- Create different versions of your design just in case you have to go back to double check your design or like in my case, to add some supports and to see if they are working. It is possible to go back in history in Fusion but it is way less convenient than to just go back in file history.
- add a bit of tolerance of about 0.3mm (check with the lab staff what they recommend in terms of material and tool)

I followed this tutorial recomended by the FabAcademy:

<iframe width="740" height="455" src="https://www.youtube.com/embed/VZU_Jpyyc5M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I followed this tutorial till he started to prepare the files for the cutter. I placed my stuff in the stock in Fusion and exported a dxf file. To prepare the files for our CNC in the lab, we used RhinoCAM.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week8/screenshots/Screen Shot 2019-04-20 at 13.53.42.png)

### design of the chair.

The design itself was quite easy. I just set the parameters first of material thickness and also the tool I would use to cut the board (we need this value later for automating the dogbone fillets). I sketched both parts like on [this picrure](https://www.etsy.com/listing/543143880/star-gazer-chair?ref=unav_listing-other-44) and extruded it with the thickness of the material. I joint the two parts to see if they fit and have the angle I would like to have. I adjusted a bit the height of the cut out to get a nicer angle.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week8/screenshots/Screen Shot 2019-04-22 at 14.34.08.png)

### design the table.

The table was a bit trickier bit, in the end, it also worked out fine. I created the table top in the dimension 35x50cm. I extruded the top and added a fillet on two sides of 12cm to create a nice round edge.

I decided to do two different designs for the leg. The one was in the shape of an arch while the two others were simple legs in an angled position.

To see how I created them and how I cut to holes, you can follow my [documentation of week3]({{site.baseurl}}/fabacademy/2019/02/06/fab-03-CAD-3D-model/), where I explain the basics of 3D design and Fusion.

After I finished the design, I placed both objects in one 3D room the see if it works:

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week8/rendering/Viking_Chair_2019-Mar-20_03-44-10PM-000_CustomizedView34378472098.png)

I showed the design Tom, who is experienced in such things, and he recommended to add some supports in the back of the chair and between the leg to avoid bendings. I added some holes and created two triangles for the chair and three bridges for the table.

### the Dogbone dilemma.

After finishing the 3D model, I wanted to lay all parts on the scale of the sheet of wood we got provided from the academy. I created a box in the size of 120x240 and height is the thickness of the wood you set in your parameters.

Then I started to place all parts separately with the comment joint planer (like in the tutorial i linked above) on the box. It was a mess because I created some joints before I had to get rid of and then it messed up parts of my design. So I first deleted all previous joints so that I hat individual parts. Then started to place everything on the box.

As recommended in the tutorial, I used the [dogbone script](https://github.com/DVE2000/Dogbone) from Casey Rogers to create some nice dogbone filets. The nice thing with this script is, it detects all edges and corners you need to "dogbone". The first time it worked but then I had to change something and it started to mess around with me. I had a couple of errors and fails. Then I uninstalled Fusion, reinstalled it, reinstalled the script but still. It starts to work for a few parts and then stops. I changed versions of fusion from Mac AppStore to Autodesk version back and for. All this didn't help so I was already thinking, I lose more time with troubleshooting than just doing it manually.

I decided to clean up the whole session: I created new sketches of each object like I would do to create a dxf file, deleted all 3D objects I created before, placed the sketches on the stock, extruded them so I created new components and then run the script (AppStore version). Finally, it worked. But it was a mess.

Conclusion: You can save time, but clean up your session. It can confuse some automation as it seems.

&nbsp;

## Prepare the CAM file.

To prepare the file, I exported all components as one dxf file and load them into RhinoCAM. We had assistance to prepare the file, but roughly it was about

- check what machine you are using
- set up stock size
- select the tool you are using
- add the holes for the crews in the design to make sure you won't cut screws with the mill
- adjust all parameters for holes, pockets and cuts
- create the bridges
- create a live view to check if the parameters are right

For a more detailed description watch, this tutorial or I can recommend [Jessica Guys](https://mdef.gitlab.io/jessica.guy/fabacademy/computer_controlled_machining/) documentation.

<iframe width="740" height="455" src="https://www.youtube.com/embed/LQdsXYhWWDk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week8/photos/IMG_4919.jpg)

## Cut the file.

After preparing the cut and check everything in the preview, we moved to the cutting computer.

We placed the stock in the CNC, checked the mill if it is the right one for the material and the design, focuses the mill and run the first part of the project to cut the holes for the screws.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week8/photos/gCode_run.gif)

Then we screwed the stock to the CNC table and started the rest of the program. Know you see the gCode running on the computer while the machine is cutting.

Be careful: always use goggles, never stand in the way of the bridge and know always where the red emergency button is.

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week8/photos/57495472082__61F9EA5A-4186-4394-B1A1-F40B7590E68B.jpg)

## Finishing everything up.

After the cut was finished, we removed the stock from the table and saw all the left bridges to pop out the parts. Then we sanded everything and put all the parts together. I had to sand a bit more as I forgot to add the tolerance. Addiotnaly, something went wrong in RhinoCam. One cut was double the size at it supposed to be. So I had to add some wood to close the gap. But it worked.

[Download Fusion360 file.]({{site.baseurl}}/assets/fabacademy/week8/files/Viking Chair cleaned up v6.f3d)

I didn't link the RhinoCAM file as it was wrong and during the time the CAM computer was offline, I couldn't store the files in the cloud and lost them. I will hand them in as soon as I have a chance to redo them.