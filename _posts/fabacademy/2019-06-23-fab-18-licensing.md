---
layout: post
title:  "fab 18: licensing."
date:   2019-06-23 10:00:00 +0530

categories: fabacademy








---

# Licensing.

## Licensing: Creative Commons

[Phone Farm(ing)](mdef.gitlab.io/gabormandoki/masterproject) will be distributed under the Creative Commons licence Attribution-NonCommercial-NoDerivs (Creative Commons, n.d.). The project will be available for copying and sharing as longs as it is following the license terms which include appropriate crediting, non-commercial use and no own derivates.

The reason for this licensing is that every project, like DIY farms, for example, should be built with the minimum requirement of contributing to the existing network.   

In this way, the blueprint and programs can be used to build own nodes of the network, but it is not allowing to make the own network based on Phone Farm(ing). Later in the process, this possibility might be released, but the success of this system also relies on the number of nodes.  



## Open-source elements used for Phone Farm(ing) 

For the implementation of Phone Farm(ing), different already mentioned open source components will be used. 

### Golem Network

The Golem Network is a project for an open-source computing power platform. Right now, it is in a beta phase and only focusing on the home computing sector. According to Wiktor Nowakowski from Golem, the protocol this network is based on, is platform agnostic, meaning it is portable to smartphone operating systems.   

### Ethereum Network

Ethereum is an opensource blockchain network that allows developing decentralized apps (DAPP) on its platform. It will be used to provide a decentralized infrastructure for Phone Farm(ing), to issue the Attention Coins as well as to generate smart contracts for transactions. 

Blockchain is used because of multiple aspects. First of all, the privacy of farmers has to be protected as they provide sensitive data from their device. All data collected has to be anonymized and untraceable to the real identity of the contributor. Second, the whole system is decentralized as the community owns it. The community provides the sources, selects and hosts the projects without having one central power to control any of these aspects. An additional factor to this decentralization is transparency. Every transaction, project and process is available. The third aspect is the Attention Coin token, which takes advantage of the blockchain based cryptocurrency technology available. 