---
layout: post
title:  "fab 07: electronics design."
date:   2019-03-07 10:00:00 +0530
categories: fabacademy





---

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week7/photos/IMG_4868.jpg)

# Electronics Design.

------

&nbsp;

## Designing the PCB.

Before getting started, we got another class in [introduction to electronic](http://fabacademy.org/2019/labs/barcelona/local/clubs/electroclub/electronics2/) parts as this knowledge is essential. This guide is needed also for troubleshooting your board as you will find all the data sheets related to the given challenge. 

After wrapping up our electronic-knowledge, we went to the basics in designing PCBs. We had two options. One was to use [Autodesk Eagle](https://www.autodesk.com/products/eagle/free-download) the commercial version or the open source variant called [KiCad.](http://kicad-pcb.org) Both software's are free to use. I decided to use Eagle, as I got used to Fusion 360 over the last couple of weeks and hoped that it would have a similar environment. It worked, but the software has its weaknesses. So probably I will give KiCad a shot next time. 

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week7/screenshot/Screen Shot 2019-03-06 at 17.35.31.png)

&nbsp;

So after downloading and instaling Eagle, we started with adding the fab component library. The reason is this library only contains parts available in the lab. So stick to that in your design. I made my life easier by unchecking all other libraries so that only the fab library is in use.

&nbsp;

#### tip:

*So first download the fab Libary [here](http://fabacademy.org/archives/2014/tutorials/electronics/fab.lbr), go to "Libary" in the top menu and select "Open Libary Manager". Click on the "Available" register card, click on "Browse" and select the downloaded "fab.lbr" file. Now, select the file in the list and click on use. The fab library is now available in your components manager. So now comes the step that makes your life easier: Click on the middle register card, select all libraries in the list, unselect only the library called "fab" and than click "Remove". Now the only components you will find in your components list are components available in the Fab Lab. No worries you can readd the other libraries anytime. They are still available in the "Available" list.*

&nbsp;

#### components.

All set up, we start to add all the components we would like to have in our design. The template we are using for the basic stock of the components is "Hello Echo" board from the fab academy.

You will find there the following components (you will find them all in the fab library):

- [ ] 6-pin programming header
- [ ] Attiny44A microcontroller
- [ ] FTDI header
- [ ] Crystal 20Mhz
- [ ] resistor 499 Ohms
- [ ] resistor 10k Ohms
- [ ] button 6mm switch
- [ ] LED (in my case red to fit the 499 resistors)

I added the following components to my design:

- [ ] 2x capacitor 20pf
- [ ] 3x resistors 0 ohms as jumpers

Components I missed in my design and had to add later. **Be careful when orienting on my design. There is something missing! I updated the Eagle files but haven't tested if it is working!**

- [ ] another resistor 10k Ohms
- [ ] capacitor 10uf

You are free to add any component you would like to have as longs as you have an input (button) and an output (LED).

Click on the little plus cable symbol in the tool menu to add components. [Here](http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week6_electronic_design/eagle_english.html) you will find a detailed Fab Academy tutorial for Eagle. [Here](https://gitlab.fabcloud.org/pub/projects/tree/b4c35d75544dc1eb4b16d917af75a97b7221cab8) you can find all resources available for electronic production.

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week7/screenshot/Screen Shot 2019-03-06 at 17.34.55.png)

&nbsp;

After adding all the components to your board, you have to connect them. By adding the right names to the connections, you are connecting them automatically without drawing a "physical" line. *(See in the tutorial).*

Make sure every and each connection is named or manually connected. Then you can change to the board view. Click on the white/green icon in the top menu bar to switch to the board view.

You will find your components in the bottom left corner. Start to place them on your board (inside the yellow border).

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week7/screenshot/Screen Shot 2019-02-28 at 15.49.29.png)

&nbsp;

This part now is a lot of trial and error. Be patient and most importantly not tired. Try to place the different components strategy so the connection is easier. You see here a really messy example I started with. And another warning:

#### Check if you have all components! Then check again!

I did a beautiful design and connected all the parts. Then I realised that three essential parts were missing. You can be lucky and you don't have to redo the whole design, but usually, it will be a mess. 

After finishing your design, clean it up. Make sure the lines and connections are clean and look nice. That they are straight and not bent. Now you can change the size and shape of your board by changing the dimension layer and adjust the yellow border. My board resulted in this design (remember this design is missing a component):

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week7/screenshot/Screen Shot 2019-03-06 at 17.34.45.png)

&nbsp;

So, having your design ready, you have to test or check it with to fab academy rules. For that, you have to download the [fabacademy design rules](https://github.com/Academany/FabAcademany-Resources/blob/master/files/fabmodule.dru). It will check if the traces have the right distance if all components are connected etc. 

#### [schematic view.]({{site.baseurl}}/assets/fabacademy/week7/files/Hello Echo Gabor.sch)

#### [boardview.]({{site.baseurl}}/assets/fabacademy/week7/files/Hello Echo Gabor.brd)

&nbsp;

After checking all the warnings (I guarantee you will have some) you can export the file as PNG. You have to export the traces and outline in different files. You can do it by making only the 1st layer visible for the traces export and to dimension layer for the outline export. The values are 1000dpi and monochrome. Now your file is ready to mir prepare with [fabmodules](http://fabmodules.org). 

#### [traces.]({{site.baseurl}}/assets/fabacademy/week7/files/traces_Hello Echo Gabor.png)

#### [outlines.]({{site.baseurl}}/assets/fabacademy/week7/files/outline_Hello World Gabor.png)

#### *If you are using my files, remember to add the missing component! I haven't exported the png again.*

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week7/photos/milling.gif)

*You will find a detailed documentation about milling [here]({{site.baseurl}}/fabacademy/2019/02/26/fab-05-electronic-production.-updated/).*

Before starting to mill, check the dimensions of the exported file. I experienced by exporting it 1000dpi, the exported file was double the original size and I had to rescale it in illustrator.

&nbsp;

Here is the result:

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week7/photos/IMG_4868.jpg)

&nbsp;

After milling the board, I realised that the 10k resistor is missing. But I wanted to fix it. I will now walk you through how I did this. 

The plan was to add a cable between the microcontroller and the 6 pin head on the RST line. Then to "hang" the resistor directly on the VCC line and place the cable on the other side. You will see in the photos. 

So I started with the microcontroller and the 6 pinheads. The next step was to add the resistor to see if I could attach it directly to the tiny VCC line. 

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week7/photos/IMG_4869.jpg)

*On the right, this is the spot between the microcontroller and the 6 pins we have to add the cable. On the left, we added the resistor directly to the VCC line.*

&nbsp;

I did go on soldering all the components. You will find instructions for that in the same tutorial as you find the milling.

After finishing, the next challenge was to add the cable. It was really difficult in the first place and the connection was very fragile. I found it way more easy to give the cable a little bend so that it was parallel to the board and so solder a bigger surface. That worked out pretty well in the end. 

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week7/photos/IMG_4871.jpg)

&nbsp;

I tested all connections with the multimeter and everything looked fine. So I turned to the next step: the programming.

&nbsp;

## programming. 

&nbsp;

Basically, the programming should have worked the same way as for [this assigemnt]({{site.baseurl}}/fabacademy/http://127.0.0.1:4000/gabor.mandoki/fabacademy/2019/02/26/fab-05-electronic-production.-updated/) expect that this time we are using, own programmer. I followed [this](http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week8_embedded_programming/attiny_arduino.html) fab academy tutorial. I installed al the drivers and connected both boards. But I failed to burn the bootloader. I got an error message every time. 

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week7/photos/IMG_4875.jpg)

&nbsp;

I doublechecked the connections with the multimeter and found no problem. I connected again and was a bit too motivated to connect the FTDI to the 6 pins and ripped it off my board.

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week7/photos/IMG_4876.jpg)

&nbsp;

I was lucky that the copper parts were not ripped off. So I just could add a new component. So tip:

#### Be careful with the connection and solder it properly!

I could not find out what the problem is yet, I can still not burn the bootloader. So I have to doublecheck my design and go through everything with support. I will update this documentation as soon as I worked everything out.

&nbsp;

------

### update:

## troubleshooting.

As it turned out, I missed another component: a 1uf capacitor between VCC and GND. I was told to attach it to the board close to the microcontroller. I used the curve at the bottom of my design to place the capacitor. 

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week7/photos/IMG_4883.jpg)

&nbsp;

In addition, I had to fix the 6 pin I have broken before. It broke again and I ripped off the copper for the GND pin. A added another wire and now it looks like this:

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week7/photos/IMG_4884.jpg)

&nbsp;

It is not the most beautiful board for sure, but I wanted to fix it rather than to mill again. And it's possible, the connections seemed to be solid. 

Now, I went back to programming. I connected both boards again, clicked on Burn Bootloader and it still failed. So I already thought I have to start to unsolder step by step to find the weak part. Before doing that, I got the multimeter again and checked the flow between every connection going to the microcontroller. First I checked if any pin is shorted. This was fine. And then I found the problem: 

While checking all the connections I made a major mistake. I checked all pints and soldering points but I wasn't accurate enough to check the connection directly to the microcontroller pins. So I found the weak connection in the end between the 2x3 head Pin from MISO to the microcontroller MISO. Resoldering this part fixed the problem and I was able to program.

#### Steps.

First, burn the bootloader (tools > burn bootloader). 

When this went well, load the Button sketch from the example menu and modify the button and input pin to your design. You find a sheet to define your pins here:

![img]({{site.baseurl}}/assets/fabacademy/week7/photos/NmFu41u.png)

&nbsp;

My buttonPin is 3 and the ledPin 7. So my modified code looked like this:

```c++
/*
  Button

  Turns on and off a light emitting diode(LED) connected to digital pin 13,
  when pressing a pushbutton attached to pin 2.

  The circuit:
  - LED attached from pin 13 to ground
  - pushbutton attached to pin 2 from +5V
  - 10K resistor attached to pin 2 from ground

  - Note: on most Arduinos there is already an LED on the board
    attached to pin 13.

  created 2005
  by DojoDave <http://www.0j0.org>
  modified 30 Aug 2011
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Button
*/

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 3;     // the number of the pushbutton pin
const int ledPin =  7;      // the number of the LED pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // turn LED on:
    digitalWrite(ledPin, HIGH);
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
  }
}
```

&nbsp;

Upload the code and when everything is alright you should be able to control the LED with the button.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week7/photos/blink.gif)

&nbsp;

### conclusion.

Double check if you have all components, be careful with soldering, check all connections accurately and try to fix every issue with wires and components before milling the board again. You will learn way more by troubleshooting. 