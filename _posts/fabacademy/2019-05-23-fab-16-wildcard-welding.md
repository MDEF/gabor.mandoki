---
layout: post
title:  "fab 16: wildcard - welding."
date:   2019-05-23 10:00:00 +0530

categories: fabacademy







---

![]({{site.baseurl}}/assets/fabacademy/week16/IMG_5240.jpg)

# Wild Card: Welding.

------

In the wildcard-week, we had the freedom to choose between different activities only our lab could provide. In my case, I choose to weld. Before starting, we had a short wrap up about safety. We got green coats we should wear out of fireproof material (in the photo above its did the wrong way). The second thing you have to wear all the time as soon as you are welding is the mask:

![]({{site.baseurl}}/assets/fabacademy/week16/IMG_5239_Lively.gif)

There are two models in the lab: passive and active.

The one who welded always wore the active one. These are darkening automatically as soon as you start to weld, while the passive ones are always dark. So you have to list them anytime when you stop welding.

To not to burn yourself, the third safety tool was the leather gloves. They also affect that you are isolated from the electric circuit as it always runs through the piece and the welding head (that is how the heat is produced, electricity = energy = hot)

From then on, we were guided through the steps. First, you have to fix the pieces you want to weld to the table with clamps. After both pieces are set (flat as possible), you have to attach the ground to one of this to make the circuit possible.

The workspace is now set up. The next is the setting of the tool.

![]({{site.baseurl}}/assets/fabacademy/week16/IMG_5248.jpg)

You see different values on the device. The voltage, the ampere and the speed of the wire coming out of the gun. We adjusted the settings a couple of times and ended up with the settings you see on the photo. But I think its a bit try and error anytime. There is no right or wrong.

The gun itself had a trigger. If you pull the trigger, the wire in the middle of the nozzle drives out. This is the material used for welding. When it touches the surface of the parts you want to weld, it melts because of the electronic circuit. Check the wire before you start. If it is out too much, cut it. If it has a blob on the front, cut it. It should be our around 1-2cm.

**!Be careful: don't step on the tube that connects the gun to the machine. The wire is inside, and if it breaks it is a mess to get the broken wire out again.**

So now we can start to weld. **!Attention! before you start, say something like "Ready?" so that everyone around you outs down the mask!**

So some things you should know for welding:

- your aim is to have a nice even worm as welding line

- to achieve that you have to have a steady speed

- you should also make gentle smooth circles while going along the welding path

- you can hear if you are doing well:

  - smooth, steady sound: probably good

  - cracks and inconsistent sounds: probably bad

    You will find a sound example for [good]({{site.baseurl}}/assets/fabacademy/week16/good_sound.m4a) and for [bad]({{site.baseurl}}/assets/fabacademy/week16/bad_sound.m4a) here.

![]({{site.baseurl}}/assets/fabacademy/week16/welding.gif)

Surprisingly, most of us did pretty well for their first time welding. Most of us had a nice and even weld.

After finishing the welding itself, be careful. Only touch your parts with the gloves. They are super hot.

We took our parts and ground them till the weld was nice and even.

![]({{site.baseurl}}/assets/fabacademy/week16/IMG_5246.jpg)
