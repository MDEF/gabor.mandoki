---
layout: post
title:  "fab 06: 3D printing & scanning."
date:   2019-02-27 10:00:00 +0530
categories: fabacademy



---

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/photos/IMG_4792.jpg)

# 3D printing & scanning.

---

&nbsp;

## Designing a printable 3D model. 

The first task of this weeks assignment was to design a 3D printable object, that is not reproducible with a laser cutter or CNC.

I am experimenting with these two components right now...

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/photos/IMG_4696.jpg)

&nbsp;

…so I was thinking about to design housing for both components. The first component is a [ESP32 DEVKIT V1](https://docs.zerynth.com/latest/official/board.zerynth.doit_esp32/docs/index.html) and a [I2C OLED](https://www.ehajo.de/bauelemente/displays/187/oled-display-0-96-weiss) screen.

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/screenshots/Screen Shot 2019-02-26 at 12.21.27.png)

I designed it in Fusion360. I wanted to have the board in the bottom part and the OLED in the top part. 

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/renderings/case_ESP32_+_OLED_2019-Feb-26_01-02-21PM-000_CustomizedView23135900005.png)

I exported the files directly from Fusion as STL and opened it on the 3D printing computer. 

Here you can download it for yourself:

[TopCase]({{site.baseurl}}/assets/fabacademy/week6/files/TopCase.stl)

[BottomCase]({{site.baseurl}}/assets/fabacademy/week6/files/BottomCase.stl)

&nbsp;

## Preparing the files for printing. 

We work with the slicer application [Cura](https://ultimaker.com/en/products/ultimaker-cura-software) from Ultimaker to prepare our printing files. For that, we load the STL file first in the application. 

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/photos/IMG_4701.jpg)

&nbsp;

### My notes for Cura:

I used our [Creality CR10 S5](https://makerhacks.com/creality-cr-10-s5-review/). It took some time to heat up the plate as it is huge compared to the other printers. The settings in the following are for the specific printer and PLA.



![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/screenshots/Screen Shot 2019-02-28 at 14.33.41.png)

&nbsp;

#### Quality.

1. **Layer height.**

   1. 0.3 is rougher
   2. 0.1 is better quality

   This setting is time <> quality. If you just want to have a rough prototype choose a higher layer and save a lot of time. For a better quality "final" production, choose a smaller value, but the print will take much longer. For my print I chose 0.2mm

2. **Initial layer height.**

   How thick should the first layer, the base layer that sits directly on the plate be? Relates to the value you set in layer height.

3. **Line width.**

   Relates to the nozzle. We usually use nozzles with 0.4 diameters. But we also have 0.6 nozzles for other materials.

&nbsp;

#### Shell.

1. **Wall thickness**.

   The thickness of the wall is not a value like "mm". It's about quantity. Means how many layers the nozzle will add to this wall. For example 3 walls à 0.4 means 1.2mm wall thickness. So define the Wall Thickness in Wall Line Count.

2. **Top & Bottom Layer.**

   This describes how many layers should be added on the top and the bottom of the object to close the design. You should always add more to the top than to the bottom. For example 6 top layers and 4 bottom layers.

&nbsp;

#### Infill.

1. **Infill Density.**

   This defines how much per cent of the holes in your object should be filled. The recommendation of the maximum value to create a really strength object is 90% as 100% will lead to printing problems. 50% should be fine even if it should be solid. I used 25% and my object was pretty solid.

2. **Infill pattern.**

   I used the lines pattern but you are free to use whatever pattern. Up to you and your design. You can check in the layer view what the different patterns would mean for your model.

&nbsp;

#### Material.

All these settings depend on the material you are using. Most probably [PLA](https://en.wikipedia.org/wiki/Polylactic_acid).    

1. **Printing Temperature.**

   Depends on the material. Cura assists you with the right setting when you choose the right material in the top. For PLA, for example, it recommends 200 degrees. You can raise the temperature to 205 and reduce the plate temperature to 40 degrees. The rest should be fine when you select the material. You will always find the right temperature on the package of the material.

2. **Enable retraction.**

   You should enable retraction. This feature helps you to reduce the stringing of the printer. 

3. **Diameter**

   There are two diameters we are using: 1,75mm and 3,00mm. We use the 1,75mm usually. Check what on the printer. 

&nbsp;

#### Speed.

Usually won't change. Depends on the printer. If you want to print with a flexible material, you need to change the speed here. 

&nbsp;

#### Travel.

Adjust the way the printer travels over or through already printed parts.

&nbsp;

#### Cooling.

I would recommend enabling cooling, but put initial dan speed to 0% so that the cooler its not on for the first layer. 

&nbsp;

#### Support.

This part is essential for everything over 45° in your design. The slicer automatically calculates the necessary supports you can get rid of after the printing process. The only activate when needed.

1. **Support placement.**

   Here you have to options: **Everywhere** & **Touching Build plate.** Now, to choose the right setting you have to check if its enough to build the supports only on the build plate (outside) or if you also have parts inside your object that need support. In my design, I needed both. 

2. **Support pattern..**

   Here you can decide what pattern is used for the support. I used lines and it worked fine. The density I kept around 25 to 30%

3. **Support Horizontal Expansion & Infill Layer thickness.**

   The horizontal expansion can be a low value is it only means the added expansion horizontally. The support infill layer thickness has to be a multiple of the layer height. As smaller as easier to rip it off your model afterwards. 

&nbsp;

#### Build Plate Adhesion.

I would recommend the settings **Skirt** & **4**. These are the settings for what is happening before the printing of the object starts. With that, you print an additional trace before printing the object to make sure the nozzle is clean and the material sticks to the plate. When these dales you can stop the printing and adjust the settings.

&nbsp;

Dual Extrusion is only relevant if you use a printer with to extruders. I didn't use the last two setting options.** 

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/photos/IMG_4699.jpg)

After setting everything in Cura, I moved the file with a USB drive to the printer. And the printing started…

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/photos/IMG_4702.jpg)

&nbsp;

## the result.

It took around three hours. I left the printing overnight and checked back the next day. 

The result was really nice but had to be modified a bit to work for its meant purpose.

First, I had to get rid of the support material in the screen cutout.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/photos/IMG_4793.jpg)



![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/photos/IMG_4794.jpg)

&nbsp;

After I had to clean the sharp edges. As it turned out, the two parts are not fitting perfectly together and I made a mistake in my design. The spot for the screen was too small and the hole too big. So I had to break out the stage to have enough space for the screen. Now, I have to put in a bumper to keep the spacing steady between the two components. I will upload the design as soon as I figured out its weaknesses and improved it.

&nbsp;

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/photos/IMG_4795.jpg)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/photos/IMG_4791.jpg)

The ESP32 chip fits really nice into the bottom part of the housing.

&nbsp;

## 3D scanning.

For the 3D screening, we had different options. The easiest and common one, was to use the Kinect provided by the Lab and scan whatever you wanted. Another really attractive option was the milling machine that is also capable to scan really detailed small objects. But, I had a different approach. 

I wanted to 3D scan with the tools I have. So I wanted to use my phone or my camera and built the pictures together on my computer to a 3D model. 

The two most popular apps for this purpose Autodesk ReCap and Alice vision are only available for Windows and Linux as they are supporting NVidia CURA and macOS does not.

So the first thing I tried, was to find apps for the iPhone or the iPad that could help to 3D scan an object. And I found a few:

{% appbox appstore 1444183458 %}

{% appbox appstore 1229460906 %}

{% appbox appstore 1388028223 %}

The problem with all of these three apps was, that they need the newer iPhones with a true depth IR camera. As I only have an iPhone 7, I could not give it a try, but I think in future it will be interesting to test such a system on a device fitting in your pocket.

So I was looking for another solution. I tested the app Metashape by Agisoft. They are providing a 30-day trial version also running on macOS.

&nbsp;

## Taking pictures.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/photos/IMG_4703.jpg)

My object was a candle. I placed it on a table close to the window. I was trying to create an evenly lighted environment, but I couldn't manage. So I had to live with some shadows.

I was walking around the object and took 87 photos of it from different angles. 

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/screenshots/Screen Shot 2019-02-27 at 15.51.00.png)

I used my iPhone.

&nbsp; 

## Photogrammetric with [Metashape.](https://www.agisoft.com)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/screenshots/Screen Shot 2019-02-27 at 14.53.06.png)

First, you have to load the folder with all the photos into the app by clicking in the upper menu on **Workflow>Add folder...**

After adding the folder, you have to align all the photos. Open the **Workflow** menu again and click on **Align Photos...**

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/screenshots/Screen Shot 2019-02-27 at 14.54.11.png)

That takes a few minutes depending on how fast your computer is. 

The next step is similar. Click again on the Workflow menu and select **Build Dense Cloud.**

The result will be something like this:

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/screenshots/Screen Shot 2019-02-27 at 21.10.50.png)

The next step follows the same procedure as the ones before. Click on **Workflow>Build Mesh...**

And the last step to create is to click on **Workflow>Build Texture...**

Now, finally, a nice 3D model should appear.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/screenshots/Screen Shot 2019-02-27 at 15.15.13.png)

What we do now, you can also do before these steps as it might be faster not to calculate everything in each step.

As you see we have a lot of junk material we don't need for our object. 

Metashape allows you to select the parts you want to keep or you want to through away with the selection tool you can find in the menu bar. Just keep in mind to invert the selection by selecting **Edit>Invert selection** if you selected a part you want to get rid of before you click **crop.** You can select multiple parts by pressing the command button.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/screenshots/Screen Shot 2019-02-27 at 16.06.18.png)

After cleaning everything up, you can click through the different view options to see a shaded, textured, solid or wireframed version of your object.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week6/screenshots/Screen Shot 2019-02-27 at 15.48.43.png) 

Now we can export the object in every common 3D format and edit it with Fusion360, for example, to prepare it for printing. 

Here you will find the [STL file]({{site.baseurl}}/assets/fabacademy/week6/files/candle.stl) straight from Metashape.