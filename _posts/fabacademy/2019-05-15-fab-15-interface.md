---
layout: post
title:  "fab 15: Interface."
date:   2019-05-15 10:00:00 +0530

categories: fabacademy






---

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week15/screenshots/Screen Shot 2019-05-15 at 10.21.42.png)

# Interface.

------

## Pong with Arduino and Processing.

For this project, I wanted to create a little homage to the oldest video game on earth: Pong. We will build Pong in processing and control it with a potentiometer, read by an Arduino.

#### requirements:

- Arduino Uno
- Potentiometer
- Processing installed
- Adruino IDE or Atom with Platform I/O (we will use Platform in this case)

![img]({{site.baseurl}}/assets/fabacademy/week15/screenshots/Screen Shot 2019-05-15 at 09.48.33.png)

### Setting up Arduino

First, we create a new project in Platform IO. We name it Pong, select the Arduino Uno board (or whichever board you would like to use) and click create. This process takes a minute since Platform IO downloads the newest SDKs for this project.

![img]({{site.baseurl}}/assets/fabacademy/week15/photos/Adruino_connection.png)

Now connect the potentiometer to the Arduino board. The left pin to GND, the middle pin to A0 (or any other analogue input) and the right leg to 5V.

This setup, we have to add the code for analogue input in the main.cpp file which is the programming file we know from Adruino IDE. Upload the code, open up the serial monitor, and if you get a serial input, you are set and done.

```c++
#include <Arduino.h> // for Platform IO we have to add the Adruino libary

int sensorPin = A0;     // define the input Pin you are using
int sensorValue = 0; // ariable to store the value coming from the sensor

void setup() {
    Serial.begin(9600),    // Start up the Serial communication
}

void loop() {
sensorValue = analogRead(sensorPin); //Read the current value of the Potentiometer
Serial.println(sensorValue);                // send the value to Serial
delay(50);                                                    // Wait for a short time before repeat
}
```

![img]({{site.baseurl}}/assets/fabacademy/week15/screenshots/Screen Shot 2019-05-15 at 10.21.42.png)

## Setting up Processing

So for the Pong code, I was pretty sure that there are plenty of projects around this as it is one of the oldest video games in the earth, so I looked it up and found this code:

```java
import ddf.minim.*;
import processing.serial.*;

String serialPortName = "/dev/cu.usbmodem14101"; // add the name of your input here
Serial arduino;
Minim minim;
AudioPlayer wallSound, batSound;
PImage ball, bat, back;
float batPosition;
float ballX, ballY;
float vertSpeed, horiSpeed;

void setup()
{
  size(960,720);
  if(serialPortName.equals("")) scanForArduino();
  else arduino = new Serial(this, serialPortName, 9600);
  imageMode(CENTER);
  ball = loadImage("ball.png");
  bat = loadImage("bat.png");
  back = loadImage("back.png");
  minim = new Minim(this);
  wallSound = minim.loadFile("wall.mp3");
  batSound = minim.loadFile("bat.mp3");
  batPosition = bat.width/2;
  resetBall();
}

void resetBall()
{
  ballX = 20;
  ballY = 1;
  vertSpeed = 6;
  horiSpeed = random(-6,6);
}

void draw()
{
  image(back,width/2,height/2,width,height);

  // Move the bat
  if((arduino != null) && (arduino.available()>0)) {
    String message = arduino.readStringUntil('\n');
    if(message != null) {
      int value = int(message.trim());
      batPosition = map(value,0,1024,0,width);
    }
  }

  // Draw the bat
  image(bat,batPosition,height-bat.height);

  // Calculate new position of ball - being sure to keep it on screen
  ballX = ballX + horiSpeed;
  ballY = ballY + vertSpeed;
  if(ballY >= height) resetBall();
  if(ballY <= 0) ceilingBounce();
  if(ballX >= width) wallBounce();
  if(ballX <= 0) wallBounce();

  // Draw the ball in the correct position and orientation
  translate(ballX,ballY);
  if(vertSpeed > 0) rotate(-sin(horiSpeed/vertSpeed));
  else rotate(PI-sin(horiSpeed/vertSpeed));
  image(ball,0,0);

  // Do collision detection between bat and ball
  if(batTouchingBall()) {
    float distFromBatCenter = batPosition-ballX;
    horiSpeed = -distFromBatCenter/10;
    vertSpeed = -vertSpeed;
    ballY = height-(bat.height*2);
    batSound.rewind();
    batSound.play();
  }
}

boolean batTouchingBall()
{
  float distFromBatCenter = batPosition-ballX;
  return (ballY > height-(bat.height*2)) && (ballY < height-(bat.height/2)) && (abs(distFromBatCenter)<bat.width/2);
}

void wallBounce()
{
  horiSpeed = -horiSpeed;
  wallSound.rewind();
  wallSound.play();
}

void ceilingBounce()
{
  vertSpeed = -vertSpeed;
  wallSound.rewind();
  wallSound.play();
}

void stop()
{
  arduino.stop();
}

void scanForArduino()
{
  try {
    for(int i=0; i<Serial.list().length ;i++) {
      if(Serial.list()[i].contains("tty.usb")) {
        arduino = new Serial(this, Serial.list()[i], 9600);
      }
    }
  } catch(Exception e) {
    // println("Cannot connect to Arduino !");
  }
}
```

Install processing, copy the code, and the only thing you have to change is the "serialPortName" to the port of the Adruino. You can look it up inside Platform IO (left panel bottom toggle called Devices). In my case the Input is called "/dev/cu.usbmodem14101". Press play and control the board with the potentiometer.

![img]({{site.baseurl}}/assets/fabacademy/week15/screenshots/Screen Shot 2019-05-15 at 10.46.24.png)

## two potentiometers and Python

I found another nice [example](https://create.arduino.cc/projecthub/cmbrooks/serial-pong-72670c#toc-programming-2) that runs a pong game in Python with two controllers. So I connected another potentiometer to the Adruino, modified to code to two inputs like this:

```c++
#include <Arduino.h> // for Platform IO we have to add the Arduino library

int player1_val;
int player2_val;
const int player1_pot = A0;
const int player2_pot = A1;

void setup() {
  pinMode(player1_pot, INPUT);
  pinMode(player2_pot, INPUT);
  Serial.begin(9600);
}

void loop() {

  player1_val = analogRead(player1_pot);
  player2_val = analogRead(player2_pot);
  Serial.print(millis());
  Serial.print(" ");
  Serial.print(player1_val);
  Serial.print(" ");
  Serial.println(player2_val);

}
```

The python code uses two libraries I didn't have. So I had to install Crusor and pySerial. Cursor is a library that can handle the terminal to visualize.

`brew install ncurses`

`pip install pyserial`

The python code then looks like this:

```python
# Imports
import serial
import curses

# Constants
HEIGHT = 24
WIDTH = 80
BALL_CHAR = '*'
PADDLE_CHAR = '|'
PADDLE_HEIGHT = 4
GAME_SPEED = 100

# Classes
class Timer:

    def __init__(self, initial_time, target_time):
        self.last_reset_time = initial_time
        self.target_time = target_time
        self.elapsed_time = 0
        self.times_reset = 0

    def check(self, current_time):
        self.elapsed_time = current_time - self.last_reset_time
        if self.elapsed_time >= self.target_time:
            self.reset(current_time)
            return True
        else:
            return False

    def reset(self, current_time):
        self.last_reset_time = current_time

# Functions
def map(val, in_min, in_max, out_min, out_max):
    return int((val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)

def read_serial(device):
    #Read serial data
    raw_line = device.readline()
    line = str(raw_line)
    line = line[2:] #Remove the unicode characters
    line = line[:-5] #Remove the cariage return and newline from the end of the string
    data = line.split(" ")
    #Convert parsed strings into integers
    try:
        time = int(data[0])
        p1_pot = int(data[1])
        p2_pot = int(data[2])
        return time, p1_pot, p2_pot, True
    except:
        return -1, -1, -1, False

def update_game(left_paddle_x, left_paddle_y, right_paddle_x, right_paddle_y, ball_x, ball_y, dx, dy, update_ball):
    game_lost = False
    loss_message = "No one has lost"
    if update_ball:
        #Update ball's position based on the x and y velocities
        ball_x = ball_x + dx
        ball_y = ball_y + dy
        # Check to see if the ball has hit the right wall
        if ball_x >= WIDTH - 1:
            ball_x = WIDTH - 1
            game_lost = True
            loss_message = "Player 1 won! Player 2 is pretty sucky though..."
        # Check to see if the ball has hit the left wall
        if ball_x <= 0:
            ball_x = 0
            game_lost = True
            loss_message = "Player 2 won! Player 1 is pretty sucky though..."
        # Check if the ball has bounced off the left paddle
        if ((ball_x <= left_paddle_x) and (ball_y >= left_paddle_y) and (ball_y <= (left_paddle_y + PADDLE_HEIGHT))):
            dx = -dx
            ball_x = left_paddle_x + 1
        # Check to see if the ball has bounced off the right paddle
        if ((ball_x >= right_paddle_x) and (ball_y >= right_paddle_y) and (ball_y <= (right_paddle_y + PADDLE_HEIGHT))):
            dx = -dx
            ball_x = right_paddle_x - 1
        # Check to see if the ball hit the bottom wall
        if ball_y >= (HEIGHT - 1):
            ball_y = HEIGHT -1
            dy = -dy
        # Check to see if the ball hit the top wall
        if ball_y <= 0:
            ball_y = 0
            dy = -dy

    # Check to see if the paddle does not pass through the top
    if left_paddle_y <= 0:
        left_paddle_y = 0
    # Check to see if the paddles do not pass through the bottom
    if left_paddle_y >= (HEIGHT - PADDLE_HEIGHT):
        left_paddle_y = (HEIGHT - PADDLE_HEIGHT)
    if right_paddle_y >= (HEIGHT - PADDLE_HEIGHT):
        right_paddle_y = (HEIGHT - PADDLE_HEIGHT)

    # Erase old game
    screen.clear()
    # Draw new paddles
    for i in range(PADDLE_HEIGHT):
        screen.addch(left_paddle_y + i, left_paddle_x, PADDLE_CHAR)
        screen.addch(right_paddle_y + i, right_paddle_x, PADDLE_CHAR)
    # Draw new ball
    screen.addch(ball_y, ball_x, BALL_CHAR)
    screen.refresh()
    return ball_x, ball_y, dx, dy, game_lost, loss_message

# Start main program
try:
    serial_device = serial.Serial('/dev/cu.usbmodem14201', 9600) #change here to the port you are using
    serial_device.readline()
except:
    print("No serial device found")
    exit()

# Initilize timer after serial comminications have reset
initial_time, initial_pot, temp_bool, temp_string = read_serial(serial_device)
# timer_initialized = False
# while not timer_initialized:
#     initial_time, initial_pot, temp_bool, temp_string = read_serial(serial_device)
#     if initial_time == 0:
#         timer_initialized = True
timer = Timer(initial_time, GAME_SPEED)

# Initialize game variables
screen = curses.initscr()
screen.refresh()
ball_x = int(WIDTH / 2)
ball_y = int(HEIGHT / 2)
left_paddle_x = 6
left_paddle_y = int(HEIGHT / 2)
right_paddle_x = WIDTH - 7
right_paddle_y = int(HEIGHT / 2)
ball_dx = 3
ball_dy = 2
game_lost = False

while not game_lost:
    time, p1_pot, p2_pot, data_received = read_serial(serial_device)
    if data_received:
        left_paddle_y = map(p1_pot, 0, 1023, 0, HEIGHT - 1)
        right_paddle_y = map(p2_pot, 0, 1023, 0, HEIGHT - 1)
        is_time_elapsed = timer.check(time)
        # Update game
        ball_x, ball_y, ball_dx, ball_dy, game_lost, loss_message = update_game(left_paddle_x, left_paddle_y, right_paddle_x, right_paddle_y, ball_x, ball_y, ball_dx, ball_dy, is_time_elapsed)

curses.endwin()
print(loss_message)

```

So the result is:

![]({{site.baseurl}}/assets/fabacademy/week15/photos/pong_python_arduino.gif)
