---
layout: post
title:  "Engaging Narratives."
date:   2018-12-10 15:00:00 +0530
categories: [term1]

---

![]({{site.baseurl}}/assets/week9/priscilla-du-preez-165366-unsplash.jpg)

# Engaging Narratives.

------

&nbsp;

The week of engaging narratives challenged us to substantiate our ideas and projects. We learned and used tools to create and show the world around our fields of interest and project.

### first stage.

&nbsp;

![]({{site.baseurl}}/assets/week9/Kickstarter_Narrative3.jpg)

In the first place, we were asked to create a hypothetical Kickstarter appearance. The challenge for Kickstarter is to present a project nearly finished to create engagement of the community. As it seems to me, the minimum level is to present a "working" prototype. I presented my idea of an on-spot information assistant "Emanuel."

&nbsp;

### second stage.

![]({{site.baseurl}}/assets/week9/IMG_3870.GIF)

<a href="https://itunes.apple.com/de/app/duckling-insight-media/id1326536917?mt=8" style="display:inline-block;overflow:hidden;background:url(https://linkmaker.itunes.apple.com/en-us/badge-lrg.svg?releaseDate=2017-12-29&kind=iossoftware&bubble=apple_music) no-repeat;width:135px;height:40px;"></a>

On the second stage, Bjarke Calvin introduced us to what he calls an "insight media" platform. Duckling allows you to present stories about your work, passion, ideas or whatever in a story format we now from Snapchat or Instagram to a community. The focus here is on quality content rather than the daily private insights of the Kardashians.

I presented an app concept I developed two years ago "Trip & Plant". It is an app that shows you how many carbon dioxides your last trip by plane produced and how many trees you have to donate to offset the amount.

[link to story.](https://duckling.me/gabor/5784652315847)

I am thinking about realising this project soon.

&nbsp;

### third stage.

![]({{site.baseurl}}/assets/week9/Emanuel%20poster.jpg)

The last stage was to present the current status of our final project in an engaging narrative way. For that, I put every thought, research material, best practice, vision and fields of interest etc in a truncated format on blueprint poster to generate an overview of my status quo. *(please click the PDF button to see the full resolution poster and to read.)* [PDF](http://mdef.gitlab.io/gabor.mandoki/weeks/assets/week9/Emanuel_poster_small.pdf/)

I decided the poster into different sections:

- small handwriting: my vision of the future world.
- bigger handwriting: questions and headlines regarding the projects and the vision.
- text blocks: fields of interest and possible components of the project, as well as best practice.

I will probably use this format as well for the final exhibition of term 1 in the last week as it allows to present to the wide field of influences of the final project.



### core values.

After we missed the chance in the times when radio, tv or the internet were new technologies to decentralize the providers of information we now have a new chance to do so. There are new technologies. If we use them right, it could lead to a transparent, decentralized world with spread power to every one of us.

A lot of trouble is caused by the hidden figures and deals behind the curtains in politics and economy.
People are losing trust in the so-called „establishment“ and shifting to extreme political sides.

I believe that there is no need to keep things secret - but only if everything is transparent.

&nbsp;

#### privecy & transparacy.

As much as I believe in transparency, I also believe in privacy. For being transparent in production chains, politics or finances no-one has to give up privacy. We empowered companies by giving them our data for services which gains the feeling that life wouldn’t work out nowadays without giving up privacy. We should promote to get this power back to us and be aware of deciding what we want to give and what not.

&nbsp;

#### education.

For all this, in the end, education is key. Education has to be reformed radically.
Information is everywhere, but we have to learn how to get access and how to classify the information.
We could focus more on how to learn rather than what to learn.



### fields of interest.

&nbsp;

#### Ai.

Emanuel will learn over the time how you behave and how you handle your daily life. After a while, it will provide with useful information like an assistant.

To provide this feature, I will add a self-learning system.
Mycroft is an open source AI voice assistant which could provide the necessary elements.

&nbsp;

#### sensors.

One core element of this system is the sensor network. It provides the ability to monitor your household and habits.

There is a system called synthetic sensors from Gierad Laput. It is a  chip with multiple sensors you can install in any room. It detects changes of temperature, sound, movement, light etc. and link it to a specific
 action.

Another essential sensor will be your phone as it is perfectly equipped with a lot of sensors for light, sound, movement and position.

In addition, it is possible to think about applying an iBeacon network. It is a protocol that allows you to broadcast information directly on your phone as soon as you are nearby.

&nbsp;

#### Interfaces. Human-machine interaction.

The interface you can access Emanuel will be your phone. I will allow you to check your dashboard anytime. It will send you to push notifications about anything you do and want to know.

&nbsp;

### 5 stages.

The concept of Emanuel is based on five stages:

The first stage is all about measuring and monitoring your habits and consumption.

Stage two is about visualisation. You will find all processed data on an easy to understand dashboard.

The third stage is about information. Emanuel provides you with all data about your consumption and context to your habits.

Stage four will allow you to set goals you want to achieve like reducing energy and water consumption. It gives you the ability to improve your daily habits by achieving your goals.

The fifth and last stage will extend the range of Emanuel outside your
 household to your work, sport or any other activity.
