---
layout: post
title:  "final presentation: phone farm (ing) ."
date:   2019-04-18 15:00:00 +0530
categories: designstudio term2



---
<div contenteditable="plaintext-only"><div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/330507148?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script></div>

&nbsp;

# Phone Farm(ing).

####  [poster as PDF.]({{site.baseurl}}/assets/slides/term2_final/phonefarming_poster.pdf) (recommended)

&nbsp;

## Attention Sovereignty.

### 1. Attention Economy.

Since the 2000s, internet companies like Google, Facebook or Twitter became the few giants having the internet in their hands. A reason for that is their perfection in hooking us to their services. By using design methods like gamification, dark pattern and taking advantage of our nature as humans, they created an environment of distraction.

Many studies have shown how this constant influence on our attention has a big impact on how we interact with each other.

### 2. Why we get hooked.

Why is it so easy for these companies to get our attention? They are using a few very simple human weaknesses:

Social acknowledgement, FOMO, bottom-up influence & the strive for the next dopamine shot. These characteristics make us more or less addicted to our phones.  

‘pull to refresh‘ is one of example of applying design methods... we know the same mechanics from slot machines - and they make more money in the U.S. than baseball, movies, and theme parks combined.

![]({{site.baseurl}}/assets/phonefarming/IMG_5082.JPG)

### 3. Effect on <u>social interaction</u>.

All those influences, that hit us every day, lead to a change in  behaviour. Undivided attention becomes more and more challenging even in an intimate personal conversation.

The length of time we are capable to focus is shortened. Studies have shown that the mere presence of a mobile phone can have negative effects on closeness, connection, and conversation quality in face-to-face interactions.

At the same time, our lack of metacognition seems to make us incapable to stop getting manipulated. We are all the pets of our smartphones.

### 4. What can we do about it?

We can train our brain and change our habits. To do so, we can use the same methods used by the ‚hooking‘ companies - only reversed.

We get rewarded for not being online, for not being on our phones. We should create a natural environment to shift the purpose from distraction to contribution.

&nbsp;

![]({{site.baseurl}}/assets/phonefarming/phonefarming_graphic.jpg)

&nbsp;

## Phone Farm (ing).

### your phone is more powerfull than you think!

Did you know that a phone from today is as fast as 120,000,000 'Apollo 11' computers that actually brought people to the moon?

If we take Moore’s law into account, this capacity doubles every two years.

So why do we use this huge amount of power only to ‘distract’ us? We should add a purpose:  unplug yourself and plug your phone into one of the phone farms - to distribute this power to a bigger network.

### together we are strong!

The farms combine the power of every connected device to create more powerful computer.

The farm is a cabinet with drawers for each phone. Every drawer is equipped with a charging option and is lockable to keep the phones safely stored.

The cabinet is scalable for different purposes.

### the collective & economy.

In the cloud, the computing power of all farms is combined to a super-computer. These provides enough ressources for all kinds of projects.

The calculations for emerging technologies become more and more complex. This cloud helps to provide a more democratic source of computing power for households, schools, artist and labs to run their projects.

### distraction free zones.

Imagine yourself going to a concert to see one of your favourite artists. But you can‘t see the stage anymore. An ocean of smartphones are blocking the sight - according to the principle: '**If you don‘t Instagram it, you haven‘t done it'**.

Imagine a dinner where your companions lose themselves into their digital 2nd life rather than keeping up with the conversion.

We all know these situations.
We are distracted. All the time.
Let us create some distraction free zones together!

### in share we trust!

The farmer will be rewarded for letting the phone work and distribute the power to the collective.

The system will give attention coins which can be redeemed for discounts or free drinks at participating partners.

Additionally, the provider of the farm can reward the farmer with some exclusive media content, like photos or exclusive goodies of its services.

### noumerous of possibilities.

This system is applicable to all sorts of public or semi-public interaction environments.

Clubs, bars and restaurants can place the cabinets right next to there cloakroom while schools or universities can place the cabinets in their classrooms and also use the computing power for their own purposes.

&nbsp;

![]({{site.baseurl}}/assets/phonefarming/phoneFarm_sketch_2019-Apr-06_08-52-12AM-000_CustomizedView15009465744.png)

&nbsp;

## Application.

### how can the distributed power be used?

Nowadays phones already have 25% of the capacity of an average computer according to BOINC (Berkeley University). A network of many phones could fire up a serious amount of computing power for all types of calculations.

The golem project is a decentralized, open-source computer power network working exactly on that principle - besides that they are focused on classical computers rather than phones.

The phone farm network could be accessible for any cultural & communal project that has no capacity to do the math by themselves or to test on a higher
 variety of devices.

Machine Learning, experiments, big data calculations, app development and film renderings - just to name a few.

The options are endless and allow to build a collective economy for real-time collaboration.

### live concerts & clubs.

Concerts & clubs are spots for some of our most emotional memories. Of course, we want to keep them and remember them. But instead of living the moment and dive deep into the atmosphere, we often hide behind our black mirror to catch every moment for later. What if we could experience concerts the best possible way and still get enough material to recall the memories of this night?

If you unplug, PhoneFarm(ing) will provide you with **exclusive pictures & videos** from the event. So you don‘t have to worry about missing the chance to take a memory home or to share it with your friends.

### universities & schools.

Schools & universities are highly affected by the presence of phones. Studies have shown, that students get distracted very easy and that the time to refocus increases disproportionately.

The farm could be installed in the classroom to motivate the students to unplug for the time of studying.

At the same time, schools & universities could use the farm for their own projects.

Additional rewards could be discounts in the cafeteria.

### restaurants & bars.

UK pub chain Samuel Smith Old Brewery banned phones from their pubs and treat them the same way as cigarettes. The customers have to go out to use their phone. The aim is to encourage conversation.

Before starting to treat phones that way, phone-farms could be installed in restaurant & bars, while promoting a phone free time with some discounts or free beer for attention coins.

&nbsp;

![]({{site.baseurl}}/assets/phonefarming/phoneFarming_MockUp.jpg)

&nbsp;

## app.

#### [link to try the prototype.](https://xd.adobe.com/view/cef6b3a8-421a-40b1-450d-8ce0a3dca3e0-b8c7/)

## Attention Sovereignty in the year 2050: I/O

In 2050, phones will be a relic of the past. Devices will be more integrated into our bodies and clothes. This makes it even more difficult to escape from attention catching influences.

But one thing changed. We are aware of this. We are aware that we have to operate a switch that is integrated into every application to change the purpose.We will actively decide if we want to be distracted or if we want to channel the power of the gear into another direction.

PhoneFarm(ing) is just the beginning of this aspiration.

**‘switchable by design’** will be the approach for the devices in 2050.

&nbsp;

## Phone usage of a MDEF student.

![image-20190418210434067]({{site.baseurl}}/assets/phonefarming/image-stats.png)

deeper look [click here]({{site.baseurl}}/assets/phonefarming/phone_statistic.pdf).

&nbsp;

## a selection of references & state of the art.
---

#### projects:

#### [the golem project.](https://golem.network)

#### [the light phone.](https://www.thelightphone.com)

#### [moment.](https://inthemoment.io)

#### [center for humane technology.](http://humanetech.com)
---

#### people:

#### [Jordan Greenhall.](https://twitter.com/jgreenhall)

#### [Tristan Harris.](https://twitter.com/tristanharris)

#### [Shoshana Zuboff.](https://twitter.com/shoshanazuboff)

#### [Guy-Ernest Debord.](https://en.wikipedia.org/wiki/Guy_Debord)
---

#### selection of books & papers:

#### book: the dictracted mind by Adam Gazzaley & Larry D. Rosen

#### book: the age of surveillance capitalism by Shoshana Zuboff

#### book: evil by design by Chris Nodder

#### study: the mere presence of a cell phone may be distracting by Bill Thornton, Alyson Faires, Maija Robbins, & Eric Rollins

#### study: how the presence of mobile communication technology influences face-to-face conversation quality by Andrew K. Przybylski & Netta Weinstein

&nbsp;

## refelctions.

The first thing pointed out, was the missing model about how the economic structure could work. If I create a token, let's say on the Etherum network, how would the real value be generated? Is it a "circular economy" where no real money is needed? What are the roles of the companies and the people or projects who are using the network economically? I will work on that topic for sure to give answers to those questions. But I will limit my focus as this topic, in particular, would need a well worked out a business model. The [Basic Attention Token](https://basicattentiontoken.org) from Brave will be an example of how I could build a self-sufficient economy circle.

![]({{site.baseurl}}/assets/phonefarming/flowtheory.jpg)

The second comment was to focus a bit more on the gamification aspect of this system to create more engagement. For that comment, I was introduced to the Flow theory. It describes the perfect level between challenge and skill. I will research this theory and look for some example applications of it.

The third point was to work out how much energy is unused in phones or even in old phones and how this could be applied to more communal uses like schools.

Another question I was asked, was about the environmental impact. How much energy will this network consume? We had a bad experience in crypto mining as Bitcoin mining, for example, uses as much energy a year as 1 million transatlantic flights (Guardian). On the other hand, especially processors of phones are trimmed to be very energy sufficient. So I have to see how this is manageable and how I can propose a solution for a green energy source in every context of use.

An additional aspect added, was the cultural context. How could Phone Farm(ing) look like in countries like Japan, Brasil or China where to the context of smartphone usage is different. This will be really an additional aspect as I will focus the intervention on Barcelona and Europe first.

By applying the phone farm to places like schools, the question came up, if I could add an educational layer or workshops. This might be interesting but a different field of intervention and more related to Oliver's project what I would like to collide within another context.

The last comment I received was the question if I think I could present the phone farm in the context of the Sonar D or Mobile Wolrd Congress next year. Well, this all depends on the success of the next 2 months. But I could absolutely imagine to work out an experienceable model of this design proposal as this will be also part of the intervention.

&nbsp;

### conclusion.

I received mostly very positive feedback what made me confident that I am on the right way. From the comments, I will mainly focus on the ones I could use to do the intervention. All of them are important and it is neccesiry to consider for my updated proposal.

&nbsp;

## intervention.

In the next 6 weeks, I would like to set up an experienceable booth to some events where PhoneFarm(ing) could be applied like dinners, parties or bars. For that, I already talked to Emily, who is also setting up some dinner events as part of her intervention.

Additionally, we were thinking about setting up an "intervention" event, where a couple of fellow students who are interested could test their proposals on a real crowd.

My next step will be to sketch up a really low fight demonstration prototype and to showcase the use of the phone farms to make the intervention experienceable. Additionally, I will set up an interactive homepage (phonefarm.eu).

The goal would be to partner up with minimum one event or one institution which is interested in a 'phone free' promotion or communal distribution.

---
