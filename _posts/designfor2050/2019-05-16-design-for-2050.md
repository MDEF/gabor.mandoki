---
layout: post
title:  "Design for 2050: Prismatic Minds."
date:   2019-05-17 10:00:00 +0530
categories: term3


---

<iframe width="740" height="415" src="https://www.youtube.com/embed/_SvON89JxwM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Floating Point.

### an episode of Prismatic Minds.

Theme: ID

Object: Totem & Senses

Conflict: Hyperreality vs. Enviorment

## the story:

> It is a sunny morning of May 2050, in Lagos, Nigeria. Climate change impacted daily flooding periods. The city has a population of more than 32 million. So, adaptations on the infrastructure were needed. Floating areas were created, which needed to adapt 1 meter of water level rise. Also, artificial mountains were built, where the more privileged and wealthy people live. The flooded areas are avoided by them. They do not know about the dynamics that happen there during the night. During the day, some interactions still occur, and Amadia, a tech biologist, had some business to do in one of these floating areas that day...
> Amadia is a Tech Biologist, 31 years old, from the middle class. She works in a Myco-capsule company that trains mushrooms to clean the earth contamination. Remediating toxins from human pollution.  Amadia went to Lagos floating area, with a size of 4.3 square kilometers, because she needed to pick up some samples of a special toxic present in these lands. To get in the floating area she needs to cross a “Double Bridge”. Where she shows her ID card and then automatically the floors move her to the other side.
> After collecting the samples she needs, she is ready to go back to the mountain. She heads to the bridge. But after passing her ID, the bridge isn’t moving. She gets nervous, but there is no one around to help her. It is only a machine. And it is getting dark… She realizes that she will be not able to cross and starts to feel unsafe there. She is walking around desperately. Suddenly, in the middle of the darkness, she sees a small light. She gets attracted by that, walking towards a weird construction. She sees some people, that were not the typical she knows. You wouldn’t see in the place there, during the evening or night. They were getting organized to start one activity.
> She observes that they were setting up a space, a temporary space. People were making objects, cooking - activities that Amadia never saw before. In her society, everything is made digitally. People never manipulated or changed the shape of things manually. Handcraft was never seen. She gets fascinated by this and realizes how much she lost connection to a cultural identity from her own city. The smell, the feeling, the social gathering… Enchanted by the smell of the Ogbono soup, she finds a little can on the floor and try to capture that smell to take it home. She spends the night with that community. When the sun starts to rise and it is time to go back to the mountain, she realises that the can, her totem, is not going to last… Maybe she will come back later that night.

City: Lagos - Nigeria

- below sea level because of climate change
- Dynamic Vanice of 2050
- Ebb & Flow everyday
- Parts of cities will be flooded every night
- City and buildings are floating
- Artifical Mountains built arounf the city
- Flooded areas are to avoid for people living in the mountains
- Main Charackter: Amadia (the lightining spirit)  
  - 31 years old
  - middle class
  - eduacated biologiest
  - Works at „Mycocapsel Inc.“
  - Lives on a mountain

Once she has to go to the floiting areas of the Transients (poeple living there - like the invisbles). Siren sound signals that she has to go back before the area is flooded. Can’t go back because her ID is not working to cross the bridge. She has to stay in this area. ITs the first time that she sees how this area is flooded and how the people live during the evening / night. She comes in contact with smell, physcal objects, cooking and realises how unattacthed she is from her cultureal Identity. She wants to conserve the smell so tries to catch in a can (totem) and bring it home on the next day.

## My day in 2050

> I wake up. It was the perfect amount of sleep and feel good. My DPA (digital personal assistant) is calculating the time I should sleep based on what I did during the day, what I have to do the upcoming day and of course the quality of sleep itself. The smell of coffee reaches to me, and the breakfast is already in the printing. After having the perfect breakfast with the right nutrients, based on the analysis of my urine, I put in my contact lenses, which allow me to log in to the virtual workspace. The daily task list is presented, and I start to work. I don't have specific working hours, only to-dos and deadlines. After a while, I'm having a break and going out. I live in the city, and there are no cars around anymore. I'm grabbing my bike and cycle to the river. People are lying in grass the with goggles on. Don't know if they are here or in virtual space. I buy a Clara at a shop with no one working in it. Paying works with Face ID as everything is connected to my biometrical Data. A new message is popping in on my contact lenses. Prompting, a holograph of my friend. We are hanign out for the rest of the day, having some beers and going out for a concert. It is a tribute to "Hellacopters" with holographic copies of them on stage. I'm going home and fall asleep. 

## conlcusion

I believe that our social and cultural life won't change that much till 2050. Thinking about 31 years back from now, we are in the year 1988, a lot of things changed since then, but people are still going to concerts, drinking beers and having a social life. The areas affected the most will be work, infrastructure, digital infrastructure and education for sure. It's difficult to predict where we are heading as only one invention can change huge parts of the known system. The release of the iPhone 2007 disrupted a lot of things we got used to. There will be another big thing coming. Could be AI, blockchain, more IOT or something completely new. We will see...

