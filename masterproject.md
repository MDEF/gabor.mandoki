---
layout: post
title: master project.
permalink: /masterproject/
category: ""
---
---



<div contenteditable="plaintext-only"><div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/330507148?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script></div>

&nbsp;

# Phone Farm(ing)

####  [Full article as PDF]({{site.baseurl}}/assets/phonefarming/final/PhoneFarming_GaborMandoki.pdf) 

## Abstract

Smartphones are omnipresent in our everyday life. Sometimes even excessively present - especially when it comes to face-to-face interactions. Many studies have shown that constant distraction trough smartphones can affect us and our social life negatively. Phone Farm(ing) reduces this negative effect on social interaction by installing a network of farm-stations. The stations provide safe storage and charging for phones whilst harvesting sensor data and processing power. These sources work towards an open source cloud database and computing network that provides access to resources for projects developed for and by the community. The cloud network can be used for educational, scientific and art projects. Stations will be located in social spaces like clubs, bars and restaurants. In return for leaving the phone in the phone farm, you will be rewarded with the undivided attention and Attention Coins. This creates a motivation to detach from the phones. In the following, I will write about the subject and execution of this project.

## Attention economy and attention sovereignty

1997 Michael H. Goldhaber wrote an article
called “Attention shoppers!” in the WIRED magazine. He is pointing out that the
information economy’s real value is not information but attention. He defines
“the economy of attention – not information is the natural economy for
cyberspace” (Goldhaber, 1997). Twelve years later, Internet companies like Google, Facebook or Amazon
are the giants of our time. Their rise happened in a relatively short period
while this economy evolved. The business model invented is based on the collection of data from
users, profiling them and selling it as an advertising
service (Zuboff, 2018). To deal with the insatiability of data harvesting, new design
methodologies were invented to extend the time users spend with the services (Nodder, 2013). Billion-dollar companies perfected together with designers,
psychologists and neuroscientists the methods to grab our attention. The
challenge for us as users is to be aware of that the sovereignty to decide on
what we focus is targeted. 

## The methodology of manipulative design

The design methods used to influence users’ behaviour are mainly taking advantage of human nature. The prehistoric men were always looking for food to survive. This habit shaped us to present time. We are always looking for new "food" – in this case, information, which makes it easy to distract us (Gazzaley and Rosen, 2016). 

Our motivation to strive for more is the dopamine shot we receive for every new piece of information, in the form of notifications, news or messages. That has the net effect of creating an addiction. (Gazzaley and Rosen, 2016). In the book “The distracted mind” by Gazzaley and Rosen, there are four main factors to keep us on the screens: boredom, which moves us to look for amusement, anxiety about missing out of something, accessibility, as we all have access to these services through our devices in our pockets and lack of metacognition. Autoplay, notifications, stories which disappear after 24 hours on social media platforms such as Instagram and gamification methods like pull-to-refresh are just a few design strategies which are used to “hook” us to these platforms (Eyal and Hoover, 2014). 

Social acknowledgement nudges us periodically to check for likes and mentions. This very natural claim promotes the perception of missing out on social life if we are not consistently online. “FOMO” (fear of missing out), the term of this condition is pervasive for people with an attachment to social media. All these tools combined shaped new habits and social practises. 

## The effect on social interaction

This development has multiple impacts on a personal as well as on overall social level. Anxiety states because we might miss out of something important ('FOMO’ - fear of missing out) or tiredness because of the information overflow we receive every day (IFS – Information Fatigue Syndrome) are trends in our society on a personal level (Gazzaley and Rosen, 2016). But this constant influence on our attention has not only a negative effect on our well-being, but it also has a significant impact on how we interact with each other as many studies have shown. Undivided attention to the person next to us becomes more and more challenging, even in an intimate personal conversation. A study by Microsoft from the year 2015 shows that since the year 2000 our attention span was shrunken from 12 seconds to about 8 seconds and how this is related the overconsumption through screens (Gausby, 2015). Other trials have shown that the mere presence of a mobile phone can have adverse effects on closeness, connection, and conversation quality in face-to-face interactions. (Przybylski and Weinstein, 2013) 

How can we utilise the methods applied to catch our attention to reverse the effect and prevent manipulation? How can we use the mechanics of rewarding, social acknowledgement and gamification for community-focused purposes?  

Phone Farm(ing) is an intervention on these
adverse effects on social interaction of our phones. After analysing the
methods used to catch attention, I defined pillars the intervention is built on
to achieve a reverse effect. The first pillar is the reason. Why can being on
the phone that much be a problem? Creating awareness about manipulation
mechanics and their effect through design actions is the first phase. The
second pillar is showing other options and opportunities on how personal
devices could be used in a different contributive context. Application examples
and projects can demonstrate how a system can be built with an infrastructure
for social and collective intelligence in form of big data and computation
power. Social acknowledgement and competition are part of the third trigger,
which promotes a frequent engagement with the system. This will be executed
through the rewarding system, which is the fourth pillar. 

## Technical background

Emerging technologies like machine learning, big data calculations, biochemical simulations or film renderings are only a few possible use cases which demand a high amount of computing power. The options are endless, but the access to sources is not. 

1.8 billion smartphones were sold only in the year 2018 (IDC, 2019). These sheer amounts of computing power offer the opportunity to build a crowdsourced computing network. While these potent mini computers are designed to get our attention, we can add a more contributed purpose which allows us to create a collective economy for real-time collaboration and a democratised access to "supercomputing" power.

Nowadays, phones already have 25% of the capacity of an average computer, according to BOINC (BOINC, 2019). A network of many phones could fire up a severe amount of processing power for a variety of calculations. According to Dr David Anderson, founder of the BOINC project, around 1 million smartphones are needed to generate a computing power similar to a supercomputer. That means only 0.03% of all active smartphones are required for a severe amount of computer power.

### Computing power

![A picture containing building, indoor, floor, window  Description automatically generated]({{site.baseurl}}/assets/phonefarming/final/clip_image002.png)

To get a better idea of what a supercomputer does nowadays, I visited the supercomputer in Barcelona (BSC). The machine operates there right now is called MareNostrum4. Some awe-inspiring example projects were presented like simulations of proteins what helps to find mutations and fight illnesses, drug testing to reduce the number of people who have to go through clinical testing, simulation of wind flows to calculate the perfect mapping of wind farms or the GAIA project, which is a European wide project to map out the Milky Way (ESA, n.d.). 

Additionally, BSC is working on the Mont-Blanc project, an EU funded experimenting with ARM chips. This is the same chip architecture as used in our smartphones. The goal is to build a prototype with a very high processing capacity while reducing energy consumption radically. ARM chips are highly suitable for this approach as the development of these chips was focussing mainly on being powerful while energy efficient to fit our day to day usage (BSC, 2017). The coast for the energy consumption of the present supercomputer generation in Barcelona is about 1.5 million Euros every year. 

### Sensor data 

The sensor data the phones generate every day are already utilized in some examples. The traffic control, for example, uses the position and the gyroscope of many phones to see how fast the traffic moves. The problem is that we share this data to big companies which are doing two things with it. One thing is the better profiling of the user through movement, environment and health data. The other is optimizing services with the help of big data generated through these sensors. This effect can be defined as a “form of free digital labour”(Joler and Petrovski, 2016). 

Furthermore, an iPhone 8, for example, has a high number of sensors like barometer, gyroscope, accelerometer, proximity sensor, ambient light sensor, GPS, compass, camera and microphone. Moreover, it can scan for nearfield communication over WIFI, Bluetooth and NFC. All these sensors can generate a high amount of data with value for all sorts of projects. 

Instead of sharing it only to the service providers and phone manufactures we can create an openly accessible database for sensor data which would have two effects: we could use data for infrastructure or health projects like Silvia Ferraris “Doctors of the Future” for example, and we would automatically lower the value of individual data sets for companies that could reduce personal data trading. All data sets provided to the network would be anonymized and not retraceable to the individual user, guaranteed through the blockchain architecture of the system. 

## Example projects for the network

The Phone Farm(ing) system will be accessible for any cultural and communal project that does not have the capacity to do the calculations themselves or to run tests on a higher variety of devices. This lowers the barrier accessing computing power for educational projects, for example, as Oliver Juggins "The Puerta Project". Other projects like City Brin Labs can use data from the network to improve the infrastructure for future cities. The resources the system can provide are able to support a high variety of collective intelligence projects. Also, scientific projects like SETI, a project by NASA and Berkley University of California to look for Extra-terrestrial Intelligence, or GPUgrid for protein simulation can run on this system. 

## Farm-stations

After figuring out the technical feasibility, I focused on the realisation of farm stations as well as on the visibility of the network. The farm stations are cabinets with compartments for each phone. Every chamber is equipped with a charging option and is lockable to keep the phones safely stored. An interactive map, in the shape of the logo, will show how the power of the connected smartphones is used and the number of people participating in the network. The cabinet is modular and scalable for different purposes and locations. All stations are equipped with a WIFI access point, which is used to connect to the network and to conserve the cellular data plan of the farmer. 

## Application

Phone Farm(ing) applies to all sorts of public or semi-public interaction environments. Clubs, bars and restaurants can place the cabinets right next to their cloakroom while schools or universities can place the cabinets in their classrooms and also use the computing power and data for their purposes. 

## Distraction-free zones

### Live concerts and clubs

Concerts & clubs are spots for some of our most emotional memories. Of course, we want to keep them and remember them. But instead of living in the moment and dive deep into the atmosphere, we often hide behind our black mirror to catch every moment for later. What if we could experience concerts the best possible way and still get enough material to recall the memories of this night? If you unplug, Phone Farm(ing) will provide you with exclusive pictures and videos from the event. This way, you don't have to worry about missing the chance to take a memory home or to share it with your friends.

### Universities and schools

Schools and universities are profoundly affected by the presence of phones. Studies have shown that students get distracted very easy and that the time to refocus is by around 25 minutes (Mark et al., 2008). The farm could be installed in the classroom to motivate the students to unplug for the time spent studying. At the same time, schools and universities could use the farm for their projects and researches. 

### Restaurants and bars

U.K. pub chain Samuel Smith Old Brewery banned phones from their pubs and treat them the same way as cigarettes (Easton, 2019). The customers have to go out to use their phone. The aim is to encourage conversation. Before starting to treat phones that way, phone farms can be installed in restaurant and bars, while promoting a phone free time with some discounts or free beer for Attention Coins.

## Design and design process

### Platform

The Phone Farm(ing) platform will be built on the blockchain technology of Ethereum. The benefit of this method is a decentralized network, owned by the community, protection of privacy through encryption and secret identity, transparency of distribution and adoption as well as the ability to lunch a currency exclusively for this system.

The Attention Coin issued for Phone Farm(ing) works as a rewarding system as well as for voting and social competition. Farmers (contributors to the network) can redeem the tokens for discounts or free drinks at locations of participating partners. Moreover, the farmer can spend the tokens on projects to support them as they need to purchase the resources of the system with the Attention Coin. In this way, a democratic and transparent voting system is established on the platform. Phone Farm(ing) makes sure that only projects the community supports can run on the network. 

The farmer can also compare the number of tokens he earned with his friends. In this way, we use the mechanics of social competition we already know from other platforms, but with the goal of not being on the phone as Attention Coin represents the time away from the devices. For every minute the phone is locked in the farm station, the farmer earns tokens.


 

 

### Farm-station prototype

![A picture containing indoor, wall, table, container  Description automatically generated]({{site.baseurl}}/assets/phonefarming/final/clip_image00_farmstation.jpg)

##### photo of a farm-station prototype and voting system with Attention Coins

The prototype is a desktop version of the stations and showcases to approach of modularity and inveracity. It is divided into two main components. The component on the top is the technical compartment which houses all electronics like the Raspberry Pi, a USB charging hub and the lights. On the top end, a sheet of acrylic is applied with interactive backlighting that reacts on the number of phones connected to the farm. The second main component is the phone compartment. Each of them provides two lockable spaces for phones. All compartments are connected with 3D printed joints, which makes it easy to add or to remove compartments depending on the requirement. 

The program for the prototype is written in Python, monitors the number of devices connected and the interactive light installation  

Further instructions, a list of all required components, all files and the codes are available and GitHub with the option to reproduce the prototype.  [https://github.com/Gabor944/phonefarm](https://github.com/Gabor944/phonefarm)



### Accompanying app

![A close up of a black device with a screen  Description automatically generated]({{site.baseurl}}/assets/phonefarming/final/mock_up.png)

##### Mock-up of the Phone Farm(ing) app

The Phone Farm(ing) app is the interface for the farmer to the system. It features the status of the connection, a friends list with individual scores, a wallet for the Attention Coins, maps to find hosts of the stations, a project page where you can decide which projects you would like to support and a calendar which shows events that support Phone Farm(ing).  



## Design action and results 

### Raising awareness

One of the primary goals for all design actions related to this project is raising awareness about how society is affected by smartphones nowadays. To achieve a wide-ranging effect and the approached “being the new normal”, the mainstream has to be reached. The photographer Eric Pickersgill showed the presence of smartphones in everyday life as a motif in his series called “Removed”. He took pictures of people in day to day situations and removed the smartphones from their hands.

![Wendy, Brian, Hunter, Harper, 2014]({{site.baseurl}}/assets/phonefarming/final/Eric_Pickergill.jpg)Photo by Eric Pickersgill ©2014

Scenes like this ­let us face “the situation”.  Additionally, time monitoring software like “Moment” or the integrated “Screen Time” feature of Apple’s iOS help us to understand how much time we are spending on our devices. Therefore, I asked my fellow students if they would give me access to this information on their phones. Besides that, all students who had an iPhone and Screen Time activated were more than happy to share it with me, most were defending themselves by pointing out that this actual week was worse, meaning spending more time on the phone, compared to other weeks. As mentioned before, the numbers align with inquiries about phone usage. The average usage out of 9 phones is around 3 hours per day, activating it about 90 times per day and the number of notifications is at around 100. 

## Habits and social practice

The more complicated part after raising awareness about having an issue is to influence or even to change habits. The only way to do so is training. The challenge is to minimise 'FOMO’ and to recondition the feeling of being unattached with something positive. 

'A routinised type of behaviour which consists of several elements, interconnected to one other: forms of bodily activities, forms of mental activities, 'things' and their use, background knowledge in the form of understanding, know-how, states of emotion and motivational knowledge.' 
 (Reckwitz, 2002)

First, we have to analyse today's social practices related to the smartphone’s influence on social interaction. 
As an example, we examine the social practice of going out for a coffee with a friend. 
 Both participants are in the millennial’s generation in the age of 18-23, as they represent a profoundly affected milieu. In this image, both are sitting in a café. The smartphones are lying face down on the table. Conversations are taking place. During the whole time, the devices vibrate in irregular intervals, and one or both are checking what came in. In some situations, the participants disappear in their digital world for a while, and face-to-face interaction has completely stopped. 

These practices lead not only to interruptions in conversations but also reduces the level of enjoyment of such events. In a study from the year 2018, an experiment with 300 people showed that the presence of phones during meal situations undermine the usual benefits we have through social interaction (Dwyer et al., 2018).

The circumstance of having the phone present, or at least semi-present around us most of the times is distractive and impairs face-to-face interaction.

The desired future practice is to create the same social situation as going out for a coffee, but with a very natural routine to put down the phone to a farm like the coat to a coat rack when entering a social space. To achieve this goal, Phone Farm(ing) offers three things. One is the sheer option to lock and charge the phone out of sight in these facilities similar to the charging points you find at airports around the world. The second will be showing how the phone contributes to the community in the meantime. And thirdly, will be a rewarding system to increase motivation.  



<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/344114524" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<p><a href="https://vimeo.com/344114524">Phone Farm(ing) Experience Dinner Reflections</a> from <a href="https://vimeo.com/user92093114">G&aacute;bor L&aacute;szlo M&aacute;ndoki</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### Collective Edibles experience dinner

The first design action was the creation of a trial scenario to analyse people’s reactions and to open up discussions around that topic. 

Together with Emily Whyman and her “Collective Edibles”-project, we hosted an experience dinner at restaurant LEKA end of May. LEKA was the perfect location as it is known for its open source ethos. All recipes and furniture is available online for reproduction. Before the meal started, every guest was asked to put his phone in a black box. The box was then brought in the back of the restaurant so that the devices were entirely out of sight. Before taking them, I announced that I would take pictures and provide them right afterwards for sharing. 

During the dinner, I asked from time to time if someone is missing their phone. Reactions were divided into no and a maybe a bit. Only once or twice situations came up, that one of the guests wanted to show something on the phone to another. But the overall scene was a dinner full of conversations, and the general feedback was positive. 

As soon as I gave back the phones after dinner, every participant immediately disappeared in the digital world and checked how many notifications were received over the time of the feast. This situation was in total contrast to the rest of the night. 

![img]({{site.baseurl}}/assets/phonefarming/final/goodybag.jpg)

##### Goody bags with flyers and cookies

After dinner, we sent out a survey. Two-thirds of the guest said that they were affected by the fact being without the phone during the meal. Also, two-thirds admitted that they had the urge to check social media at least sometimes. One third said that they hadn't had the urge at all. Every participant answered yes to the question if it would make a difference if the phone is contributing to education, science or art projects for that time. The same share answered yes to the question if they could imagine using Phone Farm(ing) in social spaces in the future. The question regarding rewards was individually answered. One third said that they don’t need a reward at all. Others asked for an asset that could be used in a transaction, for example.

We interviewed our guest a few days after the dinner about the experience. Most admitted that it made a difference not having the phone during the dinner. Additionally, the fact that the pictures were sent out after the dinner was pointed out as proper compensation for not being able to take photos during the dinner. You will find the gallery and a Spotify-playlist which was created as an additional reward following this link: [phonefarm.eu/gallery](http://www.phonefarm.eu/gallery)

An edit of the recorded interviews will be released on [phonefarm.eu](http://www.phonefarm.eu/).

The outcome of this event was a very successful first shot with room to improve. Part of the experiment was only to give minimum context to see the reactions about the fact that there are no phones in the game only. But next time, I would provide more information beforehand also to promote a discussion about the framework. I will also create a showcasing voting system for the next event where every participant can vote for a project the network is working towards. 

![A picture containing indoor, wall, floor, object  Description automatically generated]({{site.baseurl}}/assets/phonefarming/final/votingsystem.jpg)

##### A prototype of the voting system with Attention Coins

Furthermore, a second event is in planning. This time, a prototype will be placed, where every guest will be able to lock in the phone by themselves as well as keeping the opportunity to access the smartphone anytime. The voting system will be set up as well, as mentioned before. I expect more engagement with the possible projects and also the desired competition among farmers about who can lock away the phone for a longer time. Moreover, single-use cameras will be issued for taking pictures of the event and to avoid the feeling of missing out of opportunities to catch memories.  

As future design actions, I propose to offer these kinds of events together with Collective Edibles as an Experience on Airbnb to reach out to a broader range of people. [Airbnb Experiences Barcelona](https://www.airbnb.com/s/experiences?refinement_paths[]=%2Fexperiences&query=Barcelona%2C Spain&search_type=SEARCH_QUERY&place_id=ChIJ5TCOcRaYpBIRCmZHTz37sEQ)

# Sustainability model

The sustainability model of Phone Farm(ing) is built on four pillars: the blockchain based token Attention Coin as currency for every transaction, the farmers who are contributing to the network, the hosts who are hosting the farms and the runners who are deploying their projects on the network. 

![img]({{site.baseurl}}/assets/phonefarming/final/Attention_Coin.png)

## Attention Coins

Attention Coin (AC) is a blockchain based token especially emitted for Phone Farm(ing). It will be built on the Ethereum network, a decentralised, open source blockchain platform, that allows you, besides other features, to create your token (Ethereum, 2019). 

The number of tokens a farmer earns represents the time spent entirely focused on social interaction. This quantitative measurement is directly convertible into a qualitative measure as we rate time away from the phones in social spaces as a desirable circumstance. Tristan Harris, a founder of the Center of Humane Technology, presented a method of how the quality of service could be measured. With the example of Couchsurfing, he described a model where every design goal had to be linked to the value generated through the rating the user assigned to an experience the service provided - in this case sleeping at a strangers place, combined with the time spent on the online platform what is considered as “cost of peoples life”(Harris, 2014). The resulting score represents the success and quality of the service rather than the quantity. 

The Attention Coin embodies a similar way of valuation. The rating of the token depends on the supply and demand of the resources from the Phone Farm(ing) network. If the rating is high, more people are distributing to them, and this equals to the time detached from the personal device. This also means at the same time that there are more resources available on the network, and more space can be purchased with fewer tokens.  

## Farmers 

By placing their phones in the farms, farmers are distributing power and data to the network. In return, they receive Attention Coins, depending on the time their devices are connected to the farms. These tokens can be redeemed for rewards the hosts provide like discounts on concert tickets or drinks. Additionally, farmers can assign Attention Coins to projects they would like to support. In this way, the community decides which projects are running on the network as Attention Coins are needed to purchase computing power or data from Phone Farm(ing).

## Runners 

To take advantage of the Phone Farm(ing) resources, runners deploy projects for the network. Everyone can be runner, schools or universities, a one-person developer, artists, city councils or labs. Runners can purchase space and data from the network with Attention Coins. They can earn the tokens trough distributing to the system in the first place or buy them from hosts. A third way is to be funded by the community. Farmers and hosts can assign their coins to projects they would like to support. 

## Hosts

Hosts are facilities which are providing Phone Farm(ing). The farms are placed mainly in social spaces like restaurants, bars, cinemas and clubs. Educational spaces like schools and universities can host them as well. Especially for the commercially driven locations in the gastronomy and entertainment field, the interest will be in being part of the network of exclusive places providing Phone Farm(ing) as a service. The goal is to set incentives for the hosts to pay for renting the farms.

Additionally, the hosts will be asked to offer some discounts in exchange for Attention Coins or to provide other rewards as exclusive media content, for example. This will also lead to a reduction in the price of renting. To achieve this goal, it is necessary to create a high demand for Phone Farm(ing) in such places. A certificate will be invented that marks hosts as such – similar we know from TripAdvisor or HappyCow. Sites can not only be looked upon the Phone Farm(ing) app but also on TripAdvisor or Google maps by filtering if they only host providing the farms or are also places to redeem Attention Coins. By using service as TripAdvisor, the effort is reduced to reach out to a high number of people as it is already a well-established community. In this way, the adoption by the mainstream will be accelerated. 

## Slack-economy

The Phone Farm(ing) network is based on the idea of slack in the ecosystem. Slack is defined as “(…) a measure of the number of unemployed resources.” (Thoma, 2014). These unemployed resources in our system are the idle processing power and the generated sensor data of the smartphones. The potential is huge. If we take the three hours of daily average smartphone usage (Turner, 2018) as a measurement, it means that about 80% of the time smartphones are not in use and so slack in the ecosystem. We already have this discussion with cars, which are standing around the main time of their life span and occupy valuable space in our cities. But it is easier to distribute the vehicle to the sharing economy than a smartphone as a car is less personal. Because of this fact, Phone Farm(ing) is not promoting sharing the device itself to a broader range of users, but its capacities while still maintain the conception of a personal device.   

## Licensing: Creative Commons

Phone Farm(ing) will be distributed under the Creative Commons licence Attribution-NonCommercial-NoDerivs (Creative Commons, n.d.). The project will be available for copying and sharing as longs as it is following the license terms which include appropriate crediting, non-commercial use and no own derivates.

The reason for this licensing is that every project, like DIY farms, for example, should be built with the minimum requirement of contributing to the existing network.   

In this way, the blueprint and programs can be used to build own nodes of the network, but it is not allowing to make the own network based on Phone Farm(ing). Later in the process, this possibility might be released, but the success of this system also relies on the number of nodes.  

## Open-source elements used for Phone Farm(ing) 

For the implementation of Phone Farm(ing), different already mentioned open source components will be used. 

### Golem Network

The Golem Network is a project for an open-source computing power platform. Right now, it is in a beta phase and only focusing on the home computing sector. According to Wiktor Nowakowski from Golem, the protocol this network is based on, is platform agnostic, meaning it is portable to smartphone operating systems.   

### Ethereum Network

Ethereum is an opensource blockchain network that allows developing decentralized apps (DAPP) on its platform. It will be used to provide a decentralized infrastructure for Phone Farm(ing), to issue the Attention Coins as well as to generate smart contracts for transactions. 

Blockchain is used because of multiple aspects. First of all, the privacy of farmers has to be protected as they provide sensitive data from their device. All data collected has to be anonymized and untraceable to the real identity of the contributor. Second, the whole system is decentralized as the community owns it. The community provides the sources, selects and hosts the projects without having one central power to control any of these aspects. An additional factor to this decentralization is transparency. Every transaction, project and process is available. The third aspect is the Attention Coin token, which takes advantage of the blockchain based cryptocurrency technology available. 

## Future projection

### Short-term – In two to five years

In two years, the Phone Farm(ing) network is finally launched. A substantial number of Farms are placed in specific spots around Barcelona like restaurants, cinemas and concert halls. The app is released, and a community of farmers is rising. Schools in Barcelona are taking advantage of the network to teach the students emerging technologies. As the power of the phones doubled compared to today, and more sensors were added an even higher variety of projects are feasible. 

Educational and artistic projects like “The Puerta Project” (Juggins, 2019) are starting to collaborate with the network. Rural regions and municipalities discover the benefits to use the system for their schools and infrastructural calculations. Phone Farm(ing) gives access to the power of a supercomputer for everyone.

A service platform is launched where everyone can order or rent farms for their events or facilities. Other cities become part of the initiative. 

The smaller Phone Yard(ing) is released for the private household. 

Farming becomes more and more popular, and people start to enjoy the benefits of phone free zones and times in their life. Moreover, the awareness about the power of data is on the rise, and a new understanding of collective data and collective health is appearing. The datasets present a picture of the current level of the community’s well-being.  Other projects and services are jumping on the bandwagon. In some situations, it becomes socially inappropriate to be on the phone rather than not to be.

### Mid-term – In 10 years.

Phones are still there but have to hold their ground against the new wave of devices. Wearables like necklaces, watches or earpieces took over. Accessories integrated into clothes, interactive ink on our skin and autonomous vehicles with an all screen cabinet are the newest releases on the market.  These devices make it even more invisible when and how we are distracted. Moreover, they are not so easy to switch off as the integration in our lives is more and more seamless.

But in the meanwhile, the phone farms did their job! The design methodology, 'switchable purpose', is highly demanded by users and costumes. New products are certified as 'switchable' if they can be switched from an only user-focused distractive mode to a distributive, community-focused mode.   

The economy around the Attention Coin includes now a high variety of businesses and services. It became a real value to spend time and focus on certain things like shows, presentations or lectures. Organisers of events, bars and restaurants and cinemas discovered the figure of giving discounts for attention in return. And farmers spend the tokens on more social events.  

Great projects were developed on the network. Other aspects of the devices are now accessible through the system. 

Phone Farm(ing) became a multipurpose platform for sharing computer power and data. In the meanwhile, it is also not physical-only anymore. For some devices of the wearables category, Phone Farm(ing) provides an app that blocks the regular usage of the device on behalf of the user based on the location.    

### Long-term – 2050

In 2050, phones will be a relic of the past. We are in a post screen period. Devices are more integrated into our bodies and clothes. This circumstance makes it even more difficult to escape from attention-catching influences. But one thing changed. We are aware of this. We are aware that we have to operate a switch that is integrated into every application and device to transform its purpose. We actively decide if we want to be distracted or if we want to channel the power of the gear into another direction. Phone Farm(ing) was only the kick-off for this aspiration. 'switchable by design' is the approach for devices in 2050.

### Conclusion.

Defending ourselves from being manipulated and distracted became a topic in our society. It started as a weak signal, but it evolves to a trend now. More and more articles and books are published about it. It is time to start interventions on collective distraction. Phone Farm(ing) has the potential to be trendsetting in terms of motivation, contribution and benefit. It also uses persuasion methods (Augustyn et al., n.d.) the same way as the big players of surveillance capitalism do, but transparent and visible. This circumstance will lead to high acceptance in the community. 

The beneficial aspect of creating a democratised access to a high amount of computing as well as to sensor data increases the potential being established. 

Phone Farm(ing) showcases a new design method that promotes hidden features and potentials to serve the user in a different context. This method can be a design approach for future products and services to empower the user.  

## Reflection

The challenge I am tackling with Phone Farm(ing) an identified for myself for a while now. Three years ago, I started to withdraw from social media services, starting with What’s App, continuing with Facebook and finally a year ago, I also quit Instagram. I had the feeling that I’ve spent too much time with these services with only a little added value to my life. We used the term digital loitering in this context over the last year because it represents what we do most of the time on social media – that’s what they are designed for. 

I was asked a lot of time how I can connect to people in this way, especially without What's App. The only difference for me personally was that people started to reach out to me directly again if something important was discussed in a group chat that relates to me. This can only work correctly if we have the same effect as with vaccination: as long as enough people are covered, the individual is as well. 

As soon as you quit these services, you get another perspective on how excessively people tend to use them. This experience motivated me to find a way to add a purpose of using phones and to reduce the time spending on phones on social interaction. 

I am a very technophile person and always well informed about the newest releases and developments in computer technology and electronics. This circumstance made me investigate the computing capacities of our devices. As mentioned before, I discovered several projects and use-cases that are already taking advantage of it but in a minimal and hidden way.

One of the most exciting parts of this project was the reaction of my surrounding. Nearly everyone admitted, having a personal challenge with spending too much time on the phone as well as that it is an issue in our society, especially before going to bed and during social interaction. On the other hand, as much as people are bothered about the fact, it was also challenging to ask for radical changes. Quitting services or living without a smartphone for a while wasn't an option. And I also think that is not necessarily a solution. We are living in a very connected world, and this is a good thing. As Robert Thompson from Materfad Barcelona said, “One of the most psychedelic advancements people have made is technology, is the ability to be able to talk over wide distances which is physically impossible to do. The ability to see people that are not physically in front of you. That already is a psychedelic experience. (…) we are using an outside tool for this or a system, or patterns but it is not different because, in the end, it has the same law which is to bring people to together. And there is only one way to grow, and that's connection.”   

The challenge was to use the benefits of being connected, to utilize the resources and to create incentives to step back for a while without promoting the feeling of being incomplete.

Through the design process, I was analysing how we systemically got hooked to the screens. It was fascinating and also made me learn a lot about human nature as the most effective design methods start precisely there: the subconscious.  Invisible triggers make us lose full control over how and what we consume and communicate digitally. Also, this is not necessarily a problem as long as the effect isn't bothering us. But as it seems, a wide range of the mainstream is aware of it and demanding some changes. 

The difficulty is, even if you are entirely aware of the situation, well informed, also quit some of the services, you are not immune. By observing myself, I discovered that the only real difference is that you are aware of specific patterns in your behaviour, but you can't change them – maybe for a short time, but you will fall back. That’s why I think that awareness is limited in power in terms of influencing habits as soon as the practices are promoted through addiction.  

Another possible downside of this all, as more time you spend on a service, that more the service knows about who you are and what you react to. That makes you transparent and manipulable. We’ve seen one of the worst results in the Cambridge Analytica scandal 2016 during the US presidential elections. Systematic manipulation trough data we give freely to social media services. 

As I said, quitting these services is an option, but by far not for everyone. And we shouldn’t fight technology that is connecting us. But we can be aware of how our data is handled and how we can use it for our purposes. Web 2.0 came in around the 2000s and allowed the user to interact with the internet as we know it today. Since then, no one asked the question really who the owner of the data is we leave in the web – but slowly we are starting to. With the GPDR (European Union, 2018), the European Union took a massive step to empowering the user over the data he leaves in the web. But it’s still a long way to go. 

Additionally, I investigated how our social interaction is affected. As mentioned before a lot of studies have shown how influential our habits can be on it. That fact made me develop a concept for social spaces to combine time reduction and social interaction working in the same direction.

A community focused ecosystem, driven by the collective, empowering the individual. 



## links:

[www.phonefarm.eu](www.phonefarm.eu)

[github.com/phonefarm](https//github.com/gabor944/phonefarm)

eMail: [project@phonefarm.eu](mailto:project@phonefarm.eu)

[archive](https://mdef.gitlab.io/gabormandoki/masterproject/archive)

