---
layout: post
title: master project - archive.
permalink: /masterproject/archive
category: ""

---

------



<div contenteditable="plaintext-only"><div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/330507148?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script></div>

&nbsp;

# Phone Farm(ing).

#### [poster as PDF.]({{site.baseurl}}/assets/slides/term2_final/phonefarming_poster.pdf) (recommended)

&nbsp;

## Attention Sovereignty.

### 1. Attention Economy.

Since the 2000s, internet companies like Google, Facebook or Twitter became the few giants having the internet in their hands. A reason for that is their perfection in hooking us to their services. By using design methods like gamification, dark pattern and taking advantage of our nature as humans, they created an environment of distraction.

Many studies have shown how this constant influence on our attention has a big impact on how we interact with each other.

### 2. Why we get hooked.

Why is it so easy for these companies to get our attention? They are using a few very simple human weaknesses:

Social acknowledgement, FOMO, bottom-up influence & the strive for the next dopamine shot. These characteristics make us more or less addicted to our phones.  

‘pull to refresh‘ is one of example of applying design methods... we know the same mechanics from slot machines - and they make more money in the U.S. than baseball, movies, and theme parks combined.

![](%7B%7Bsite.baseurl%7D%7D/assets/phonefarming/IMG_5082.JPG)

### 3. Effect on <u>social interaction</u>.

All those influences, that hit us every day, lead to a change in  behaviour. Undivided attention becomes more and more challenging even in an intimate personal conversation.

The length of time we are capable to focus is shortened. Studies have shown that the mere presence of a mobile phone can have negative effects on closeness, connection, and conversation quality in face-to-face interactions.

At the same time, our lack of metacognition seems to make us incapable to stop getting manipulated. We are all the pets of our smartphones.

### 4. What can we do about it?

We can train our brain and change our habits. To do so, we can use the same methods used by the ‚hooking‘ companies - only reversed.

We get rewarded for not being online, for not being on our phones. We should create a natural environment to shift the purpose from distraction to contribution.

&nbsp;

![](%7B%7Bsite.baseurl%7D%7D/assets/phonefarming/phonefarming_graphic.jpg)

&nbsp;

## Phone Farm (ing).

### your phone is more powerfull than you think!

Did you know that a phone from today is as fast as 120,000,000 'Apollo 11' computers that actually brought people to the moon?

If we take Moore’s law into account, this capacity doubles every two years.

So why do we use this huge amount of power only to ‘distract’ us? We should add a purpose:  unplug yourself and plug your phone into one of the phone farms - to distribute this power to a bigger network.

### together we are strong!

The farms combine the power of every connected device to create more powerful computer.

The farm is a cabinet with drawers for each phone. Every drawer is equipped with a charging option and is lockable to keep the phones safely stored.

The cabinet is scalable for different purposes.

### the collective & economy.

In the cloud, the computing power of all farms is combined to a super-computer. These provides enough ressources for all kinds of projects.

The calculations for emerging technologies become more and more complex. This cloud helps to provide a more democratic source of computing power for households, schools, artist and labs to run their projects.

### distraction free zones.

Imagine yourself going to a concert to see one of your favourite artists. But you can‘t see the stage anymore. An ocean of smartphones are blocking the sight - according to the principle: '**If you don‘t Instagram it, you haven‘t done it'**.

Imagine a dinner where your companions lose themselves into their digital 2nd life rather than keeping up with the conversion.

We all know these situations.
We are distracted. All the time.
Let us create some distraction free zones together!

### in share we trust!

The farmer will be rewarded for letting the phone work and distribute the power to the collective.

The system will give attention coins which can be redeemed for discounts or free drinks at participating partners.

Additionally, the provider of the farm can reward the farmer with some exclusive media content, like photos or exclusive goodies of its services.

### noumerous of possibilities.

This system is applicable to all sorts of public or semi-public interaction environments.

Clubs, bars and restaurants can place the cabinets right next to there cloakroom while schools or universities can place the cabinets in their classrooms and also use the computing power for their own purposes.

&nbsp;

![](%7B%7Bsite.baseurl%7D%7D/assets/phonefarming/phoneFarm_sketch_2019-Apr-06_08-52-12AM-000_CustomizedView15009465744.png)

&nbsp;

## Application.

### how can the distributed power be used?

Nowadays phones already have 25% of the capacity of an average computer according to BOINC (Berkeley University). A network of many phones could fire up a serious amount of computing power for all types of calculations.

The golem project is a decentralized, open-source computer power network working exactly on that principle - besides that they are focused on classical computers rather than phones.

The phone farm network could be accessible for any cultural & communal project that has no capacity to do the math by themselves or to test on a higher
 variety of devices.

Machine Learning, experiments, big data calculations, app development and film renderings - just to name a few.

The options are endless and allow to build a collective economy for real-time collaboration.

### live concerts & clubs.

Concerts & clubs are spots for some of our most emotional memories. Of course, we want to keep them and remember them. But instead of living the moment and dive deep into the atmosphere, we often hide behind our black mirror to catch every moment for later. What if we could experience concerts the best possible way and still get enough material to recall the memories of this night?

If you unplug, PhoneFarm(ing) will provide you with **exclusive pictures & videos** from the event. So you don‘t have to worry about missing the chance to take a memory home or to share it with your friends.

### universities & schools.

Schools & universities are highly affected by the presence of phones. Studies have shown, that students get distracted very easy and that the time to refocus increases disproportionately.

The farm could be installed in the classroom to motivate the students to unplug for the time of studying.

At the same time, schools & universities could use the farm for their own projects.

Additional rewards could be discounts in the cafeteria.

### restaurants & bars.

UK pub chain Samuel Smith Old Brewery banned phones from their pubs and treat them the same way as cigarettes. The customers have to go out to use their phone. The aim is to encourage conversation.

Before starting to treat phones that way, phone-farms could be installed in restaurant & bars, while promoting a phone free time with some discounts or free beer for attention coins.

&nbsp;

![](%7B%7Bsite.baseurl%7D%7D/assets/phonefarming/phoneFarming_MockUp.jpg)

&nbsp;

## app.

#### [link to try the prototype.](https://xd.adobe.com/view/cef6b3a8-421a-40b1-450d-8ce0a3dca3e0-b8c7/)

## Attention Sovereignty in the year 2050: I/O

In 2050, phones will be a relic of the past. Devices will be more integrated into our bodies and clothes. This makes it even more difficult to escape from attention catching influences.

But one thing changed. We are aware of this. We are aware that we have to operate a switch that is integrated into every application to change the purpose.We will actively decide if we want to be distracted or if we want to channel the power of the gear into another direction.

PhoneFarm(ing) is just the beginning of this aspiration.

**‘switchable by design’** will be the approach for the devices in 2050.

&nbsp;

## Phone usage of a MDEF student.

![image-20190418210434067](%7B%7Bsite.baseurl%7D%7D/assets/phonefarming/image-stats.png)

deeper look [click here]({{site.baseurl}}/assets/phonefarming/phone_statistic.pdf).

&nbsp;

## a selection of references & state of the art.

------

#### projects:

#### [the golem project.](https://golem.network)

#### [the light phone.](https://www.thelightphone.com)

#### [moment.](https://inthemoment.io)

#### [center for humane technology.](http://humanetech.com)

------

#### people:

#### [Jordan Greenhall.](https://twitter.com/jgreenhall)

#### [Tristan Harris.](https://twitter.com/tristanharris)

#### [Shoshana Zuboff.](https://twitter.com/shoshanazuboff)

#### [Guy-Ernest Debord.](https://en.wikipedia.org/wiki/Guy_Debord)

------

#### selection of books & papers:

#### book: the dictracted mind by Adam Gazzaley & Larry D. Rosen

#### book: the age of surveillance capitalism by Shoshana Zuboff

#### book: evil by design by Chris Nodder

#### study: the mere presence of a cell phone may be distracting by Bill Thornton, Alyson Faires, Maija Robbins, & Eric Rollins

#### study: how the presence of mobile communication technology influences face-to-face conversation quality by Andrew K. Przybylski & Netta Weinstein

&nbsp;

## refelctions.

The first thing pointed out, was the missing model about how the economic structure could work. If I create a token, let's say on the Etherum network, how would the real value be generated? Is it a "circular economy" where no real money is needed? What are the roles of the companies and the people or projects who are using the network economically? I will work on that topic for sure to give answers to those questions. But I will limit my focus as this topic, in particular, would need a well worked out a business model. The [Basic Attention Token](https://basicattentiontoken.org) from Brave will be an example of how I could build a self-sufficient economy circle.

![](%7B%7Bsite.baseurl%7D%7D/assets/phonefarming/flowtheory.jpg)

The second comment was to focus a bit more on the gamification aspect of this system to create more engagement. For that comment, I was introduced to the Flow theory. It describes the perfect level between challenge and skill. I will research this theory and look for some example applications of it.

The third point was to work out how much energy is unused in phones or even in old phones and how this could be applied to more communal uses like schools.

Another question I was asked, was about the environmental impact. How much energy will this network consume? We had a bad experience in crypto mining as Bitcoin mining, for example, uses as much energy a year as 1 million transatlantic flights (Guardian). On the other hand, especially processors of phones are trimmed to be very energy sufficient. So I have to see how this is manageable and how I can propose a solution for a green energy source in every context of use.

An additional aspect added, was the cultural context. How could Phone Farm(ing) look like in countries like Japan, Brasil or China where to the context of smartphone usage is different. This will be really an additional aspect as I will focus the intervention on Barcelona and Europe first.

By applying the phone farm to places like schools, the question came up, if I could add an educational layer or workshops. This might be interesting but a different field of intervention and more related to Oliver's project what I would like to collide within another context.

The last comment I received was the question if I think I could present the phone farm in the context of the Sonar D or Mobile Wolrd Congress next year. Well, this all depends on the success of the next 2 months. But I could absolutely imagine to work out an experienceable model of this design proposal as this will be also part of the intervention.

&nbsp;

### conclusion.

I received mostly very positive feedback what made me confident that I am on the right way. From the comments, I will mainly focus on the ones I could use to do the intervention. All of them are important and it is neccesiry to consider for my updated proposal.

&nbsp;

## intervention.

In the next 6 weeks, I would like to set up an experienceable booth to some events where PhoneFarm(ing) could be applied like dinners, parties or bars. For that, I already talked to Emily, who is also setting up some dinner events as part of her intervention.

Additionally, we were thinking about setting up an "intervention" event, where a couple of fellow students who are interested could test their proposals on a real crowd.

My next step will be to sketch up a really low fight demonstration prototype and to showcase the use of the phone farms to make the intervention experienceable. Additionally, I will set up an interactive homepage (phonefarm.eu). 

The goal would be to partner up with minimum one event or one institution which is interested in a 'phone free' promotion or communal distribution.

------











&nbsp;
&nbsp;
&nbsp;
&nbsp;

## *mid-term.*

![](%7B%7Bsite.baseurl%7D%7D/assets/week12/screenrecording%20mycroft_highquality-0001.jpg)

# Emanuel Project.

### An assistant for managing and contextualizing information.

*click here to read the [PDF](http://mdef.gitlab.io/gabor.mandoki/assets/week12/porjectEmanuel_journal.pdf)* of the first term paper.

#### [long presentaion.]({{site.baseurl}}/assets/slides/20 Slide Emanuel Presentation/index.html)  or [*PDF*]({{site.baseurl}}/assets/slides/Emanuel_preseantion.pdf)

#### [short presentation.]({{site.baseurl}}/assets/slides/short_Emanuel Presentation/index.html) or [*PDF*]({{site.baseurl}}/assets/slides/Emanuel_presentation_short.pdf)

&nbsp;

### What is the Emanuel project?

The “Emanuel” project builds an environment that helps you to gather and manage information about your everyday life - in the right spot and the right context. It gives you a new tool to make you aware of your habits, the impact of your habits and what is happening in your direct surrounding. Different pillars like ML for analysing your routine and sources, software like the Emanuel app and hardware that lets you access the Emanuel environment are carrying the infrastructure.

Emanuel can be asked questions about certain topics and has the ability to be challenged. Start a discussion, ask for more details, dive in deeper or ask for a different perspective.

&nbsp;

### Why do we need the Emanuel project?

Sometimes only the simple amount of information sources can scare us and keep us from looking things up we are interested in. Or we source it out to companies which are not acting in our interest. Emanuel will challenge this case by creating an easy access point for information that leads you through this mesh and challenges the information bubble at the same time.  

The “Emanuel” project is creating an infrastructure in your everyday environment to make information available about everything. It assists you to put certain information into the right context while giving you the ability to extend your knowledge and create a new level of awareness about yourself and your surroundings. A lot of information is getting lost or is unrecognized because of the sheer overload and the lack of contextualization of information.

Emanuel will provide information from different sources, in the right moment, at the right spot and in the right context. We can create a new level of awareness in politics, economy, society down to our day to day behaviours.

&nbsp;

![](%7B%7Bsite.baseurl%7D%7D/assets/emanuel/franck-v-516603-unsplash.jpg)

&nbsp;

### Why do we need an assistant to provide us with such information?

Today's technology is able to dynamically adapt, develop and learn from its always changing application. It is already [proven](https://www.wired.com/2016/03/googles-ai-wins-first-game-historic-match-go-champion/), that AI can be smarter in a very limited and specialized field. A lot of AI experts believe that this technology can enhance our capabilities. This really special character is ideal to assist us to find our way through the jungle of impulses.

&nbsp;

### How can I access the Emanuel infrastructure?

Emanuel has the approach to be a ‘technology to disappear’. That means it will only be available when you ask for and disappear immediately if not needed. To achieve this goal, Emanuel uses already existing interfaces like your phone, TV or wearables to interact with it. Additionally, the project will provide a range of hardware specifically designed to be discreet without being limited in functionality.

&nbsp;

![](%7B%7Bsite.baseurl%7D%7D/assets/emanuel/ev-623589-unsplash.jpg)

&nbsp;

### How is Emanuel different from already existing personal assistance systems?

 Most of the AI assistance available today are focused on home automation, purchasing and entertainment. And one thing they all have in common is that the user is modifying his habits to fit the machine rather than the machine to the user. We are changing the way we talk to make it for the machine easier to understand us. Another thing is, that most of these machines aren’t built with the intention to serve the user best. The intention is to collect data in the most private areas like homes and so gather information they need to improve their services and business models.

 Emanuel is completely user-focused, private and secure by design. The interaction with Emanuel will happen intuitively with additional features not only voice. And Emanuel as only focused on analysing and providing information.

&nbsp;

### What are the core technologies for Emanuel?

The analysis of the habits and information is based on machine learning. It is the core technology for a personalized, dynamically adapting and analysing services.

Voice, text and gesture recognition systems will be a system for the interaction with the user.

Personal information will be encrypted and only stored on-device.

*Further information about this topic will be found here soon.*

&nbsp;

### What additional aspects will the project cover?

In addition, the project will also focus on the following questions:

- Can we train an ML system by only providing our data and process it locally?
- If we need additional data, can we access data banks focused on privacy?
- Can we challenge ourselves with such infrastructure by proactively choosing a diversity of our information sources?
- Can we promote more social and political engagement with a more enlightened society?
- Can we build a trustworthy relationship to a machine by being transparent?

&nbsp;

### How will this project be realized?

At the end of the master, I will showcase a prototype that makes the Emanuel project perceptible. For this purpose, I will create access points attached to everyday objects and scenes which contain information about them. Moreover, I will work in the Fab Academy on a smart speaker or smart frame device which will represent the Emanuel infrastructure and usability.

 For further information about the Fab Academy projects follow this [link]({{site.baseurl}}/fabacademy/2019/01/27/fab-01-final-proposal/).

The final implementation will be a worked-out proposal with a speculative design approach. Additional professions, skills, time and resources are needed to realize this project finally.

&nbsp;

### What project inspired you on the way?

&nbsp;

#### [The Ocean Protocol.](https://oceanprotocol.com)

> “An ecosystem for data economy and associated services, with a tokenized service layer that securely exposes data, storage, compute and algorithms for consumption.”

 The Ocean Protocol is a very interesting project, using Blockchain to decentralize data and to provide an infrastructure for provider and consumer of data to build a trustful relationship and empower the user of its belonging. This protocol could be relevant for Emanuel to add a secure way to exchange data with certain services.

&nbsp;

#### [Snips.ai](https://snips.ai)

An open ML platform, focusing on home automation. Privacy-focused and hackable for own purpose. This system can run on a Raspberry Pi.

&nbsp;

#### [Mycroft.ai](https://mycroft.ai)

Similar to snips.ai, but completely open source. Can also run on a Raspberry Pi.

&nbsp;

#### [Olly by emotech.](https://ai.emotech.co)

Olly is a voice assistant device developed in the UK. It has the characteristics to adapt to the user and to evolve a character over time. It has also the ability to recognize the activity of the user. The approach is to "improve the relationship between humans and technology".

&nbsp;

------

## term 2:

## generel description.

How can we use new technologies to help us to get and manage information about us and our environment? How can we use technology to empower ourselves about the selection of information? How can we use such an infrastructure to provoke a new era of enlightenment?

The majority of society is not aware of the significant things happening around them. But the information is everywhere. The “Emanuel” project is creating an infrastructure in your everyday environment to provide a new way of enlightenment. From different sources, in the right moment, at the right spot and in the right context. We can create a new level of awareness in politics, economy, society down to our day to day behaviours.

The “Emanuel” project has the approach to make you aware of your habits, the impact of your habits and what is happening in your direct surrounding. The core element is the AI assistant that analyses your daily routine, your environment and information sources to provide new aspects that gains the awareness in your daily life.

The access point to this infrastructure will be in the form of smart objects like speakers and frames as well as taking advantage of already existing interfaces like your phone or tv.

“Emanuel” can be intergraded in seamlessly in your everyday routine.

## plan for the 2nd term.

### fab academy.

I want to build a smart speaker in the Fab academy that represents the "Emanuel" project physically.

![](%7B%7Bsite.baseurl%7D%7D/assets/fabacademy/week3/renderings/SmartSpeaker-draft-v12.gif)

This smart speaker is designed to be on your nightstand. It will help you to fall asleep as well as to wake up.

Based on Emanuals analyse of your day, the speaker will provide the perfect tune or audiobook to fall asleep.
Waking you up workes in the same way. By analysing your sleep and knowing what is coming up the next day, the speaker will wake you up the best way possible. In addition, the speaker can put on a podcast or radio channel to serve you the news of the day.

#### features.

for the morning:

- AlarmClock with randomized alarm sounds. The source is a predefined playlist.
- another source could be a preselected radio station as well as a specific news podcast.
- OLED screen that only shows the time.
- snooze.

for the evening:

- a selection of audiobooks and podcast are stored.
- on request, one of the audiobooks or podcasts is played.
- additionally, you can also listen to soundscapes like rain, wind or waves.

additional features:

- a voice interaction (based on snips.ai).
- gesture control.
- integration of the “Emanuel” information infrastructure.

required hardware:

- wooden box.
- OLED screens.
- knobs & buttons.
- Raspberry Pi with a ReSpeaker Mic for Voice Control.
- Arduino for controlling inputs.
- Ultrasonic sensors or 3D gesture pad for gesture control

required software:

- AlarmClock
- music and audio file player
- snips.ai

#### resources.

[Fab Academy project I.](http://fabacademy.org/archives/2015/eu/students/kreiberg_andersen.kasper/w19.html)

[Fab Academy projct II.](http://eduardochamorro.github.io/beansreels/workshops/fabghettoblaster/fabghettoblaster.html)

[AI generated music.](https://syncproject.co)

[ML plattform.](https://wit.ai/getting-started)

&nbsp;

### material driven design.

...no connection to my final project.

&nbsp;

### next steps.

I'll start to prototype. Hardware and software. I am pretty sure that I can only push this project to a certain level and have to keep the rest as a speculative design concept.



&nbsp;

------

## term 1:

### My perspective of the future.

My vision of the future relies on awareness, responsibility and a horizontal power structure in our society all driven by education, information and technology. My all in all approach is to create a portfolio of tools promoting transparency, privacy and awareness about the environment of each and every one of us.

&nbsp;

### **Transparency.**

After we missed the chance in the times when radio, tv or the internet were new technologies to decentralize the suppliers of information we now have a new chance to do so. There are new technologies. If we use them right, it could lead to a transparent, decentralized world with spread power to every one of us. A lot of trouble is caused by the hidden figures and deals behind the curtains in politics and economy. People are losing trust in the so-called “establishment” and shifting to extreme political sides. I believe that there is no need to keep things secret in the public field - but only if everything is transparent, like an open-source government. A good example of this is the data.gov.uk project of the UK that provides open-data sets from different departments of the Government. Since 2010, the platform contains over 30.000 datasets accessible to everyone.  

&nbsp;

### **Privacy.**

As much as I believe in transparency, I also believe in privacy. For being transparent in production chains, politics or finances no-one has to give up privacy. We empowered companies by giving them our data for services which gains the feeling that life wouldn’t work out nowadays without giving up privacy. We should promote to get this power back to us and be aware of deciding what we want to give and what not. Different people and companies like Rand Hindi with the project “snips” or Tim Berners-Lee with “Solid” are working on this idea. This emerges to be the next step to regain control over data and information.   

&nbsp;

### **Information.**

The majority of our society is not aware of significant things happening around them. Not aware of the consequences of their daily routine. Not aware of what they are eating. Not aware where they leave personal information. Not aware of the complex machinery behind the echo chamber creating information monopolies. We all got used to prepared and portioned information dishes - and of course only dishes we enjoy! We gave up responsibility. But information is everywhere. There will be new ways of providing this information. From different sources, in the right moment, at the right spot and in the right context. We can create a new level of awareness in politics, economy, society down to our day to day behaviours.

&nbsp;

### **Intervention: Daily routine & household.**

I want to focus first on the daily routine and the household. This is a small but complex environment which also allows me to test everything first on myself. The next step would be to bring this outside to work, sport, shopping etc. Creating awareness of the daily routine and the consequences of the everyday habit is my approach with this project.

&nbsp;

## State of the art.

&nbsp;

### **Mycroft.**

Mycroft provides an open source voice assistant. They developed not only their own hardware but also an OS called Picroft you can add to your Raspberry Pi and build your own assistant. I also tried this system. But in this case, I didn’t use it as a voice assistant. I connected it to a Telegram bot and started to chat with it. The experience wasn’t really convenient. Some skills it supposes to have were not working. Some information has been too specific. But it was very interesting to see how an open source community is adopting such a project.

&nbsp;

### **snips.ai**

Snips is a voice assistant project founded by Rand Hindi. They call it themselves a private-by-design, decentralized voice assistant that makes technology disappear.

I tried it myself by setting it up on Raspberry Pi. You can compile your own assistant on their homepage with different skills and then upload it to your Pi. If you are used to working with the command line, the installation is easy. The quality of the voice engine wasn’t really convenient which only proves that this project is in an early stage. It is planned to release a beta version of their own hardware early 2019.

 &nbsp;

### **Solid.**

Tim Berners-Lee, the founder of the www himself, wants to reform the today’s internet to what it originally supposed to be. For that, he is promoting the project “Solid”. Solid is an open-source platform developed over the last 15 years which should empower the user again to control their personal data. He attacks today’s issues with privacy and data protection on the internet.

&nbsp;

### **data.gov.uk**

data.gov.uk is a project by the UK government launched 2010 to make non-personal open data accessible. In the meanwhile, the platform contains over 30.000 datasets from different departments of the UK government.

&nbsp;

### **Open Data BCN.**

Open Data BCN is a project of the city of Barcelona that has the approach to increase the control of the citizen over their data. The companies working for the city authorities are asked to provide their collected data to this database to make privately collected data accessible for the public.

&nbsp;

### **Replika.**

Replika has the approach to provide you with a friend that helps you to reach goals like to eat more healthy food or do more sports. It is an AI what is communicating with you daily over a chat app on your phone. I tested it for a few days. My AI, Dan and I had really natural conversations. It asked me a lot of questions to paint a picture of my personality. It reminded me every day to talk to it and tried to give me a good feeling. In the end, it didn’t catch me really and I have my concerns regarding my data as I couldn’t find out what is happening with it.

&nbsp;

## Fields of interest.

### **AI & assistance.**

Artificial Intelligence for me is one of the most promising fields in technological development. It allows us to create machines that can assist us in more or less rational areas of our life. I believe that we will end in a scenario where everyone has the ability to have his personal artificial assistant that supports you in your daily routine. What the development of AI already does, and I am sure this will be more than less in the future, is challenging our understanding of moral and ethical principles. Can we generalize ethics to all humans? Can technology take these questions from us and answer them? Is a super-intelligent machine the answer to all asked questions of mankind? Probably not, but maybe it can help us to find the answer ourselves. We had a chance in the week of Designing with Extended Intelligence to dive deeper into this field. We were challenged to develop an assistant for everyday tasks or to face certain issues of our society. These exercises and the ideas developed over this week inspired me to focus on this field.

| **book:**    | *“The Digital Ape: How to Live (in peace) with Smart Machines”* by Sir Nigel Shadbolt and Roger Hampson. |
| ------------ | ------------------------------------------------------------ |
| **project:** | snips.ai, Mycroft - both AI systems with the focus on privacy and open-source. |
| **project:** | Replika - a chatbot that behaves like a friend and helps you to improve. |

&nbsp;

### **Human-Machine  Interaction & interface**.

We interact with machines every day. And it’s getting more and more. There are different approaches and some of them are so popular, that they feel very natural, like our touchscreens. I want to have an interface for my machine that interacts with the user in the most natural way possible. That means to use already existing interaction surfaces to make it as easy as possible to integrate it in the daily routines and to avoid the requirement of learning new interaction methods. The machines should give the user the freedom to decide if and when to interact with it - a shadow existence only visible in the right moment.  

| **book:**    | *“The best interface is no interface”* by Golden Krishna.    |
| ------------ | ------------------------------------------------------------ |
| **book:**    | *“Hooked”* by Nir Eyal.                                      |
| **project:** | HyperSurfaces - turning any surface to an interface of interaction with deep learning and vibration sensors. |

&nbsp;

### **Privacy & technology.**

Technologies to regain the power of your own data are on the rise. Encryption, blockchain and VPN are only the most popular in this filed. All projects of the portfolio should include privacy and diversity of sources for information by design. This principle generates trust and security. All personal data is in the hand of the user and completely encrypted on-device. To reach that approach, I will occupy myself with the possibility to develop on the Solid platform of Tim Berners-Lee that allows the user to control its data.

| **article:** | Interview with Rand Hindi on wired.de about the snips.ai project, Google and other data collectors and how to design for privacy. |
| ------------ | ------------------------------------------------------------ |
| **article:** | *“One small step for the web”* by Tim Berners-Lee in which he announces the Solid project he worked on for the last 15 years. |
| **article:** | *“How Google Tracks Your Personal Information”* by Patrick Berlinquette on Medium. |
| **project:** | Solid - a platform that restores the power of the individual on the web |

&nbsp;

### **Information & data.**

What is information? Where is information? What transports information? What kind and amount of information can we handle? We got used to the overflow of information hitting us every day. Even if it’s filtered, which is most of the time, I, for example, receive around 200 notifications on my phone every day. And there is way more than only the phone… How should we handle this amount of information? Is the quantity or the quality an issue? The filters we use today are driven by commercial interest rather than your preferences. The algorithms of Facebook, Google and co. only show things fitting into our bubble without challenging us and our opinions. We have to ask ourselves if we want to leave the importance of providing information centralized with this companies or if we want to do something about it. The solution could be a personal assistant that helps you to organize information and news and evaluates the quality and the credibility of it. It works only for you and is only driven in your interest.

| **article:**      | *“Don’t want to fall for fake news? Don’t be lazy”* by Robbie Gonzales on wired.com. |
| ----------------- | ------------------------------------------------------------ |
| **book:**         | *“Outnumbered”* by David Sumpter.                            |
| **short  story:** | *“Bubble Universe”* by Andrew Hickey.                        |

&nbsp;

### the experiment.

I want to create an assistant for awareness by delivering information about your daily routine in the most neutral way. The assistant focuses on getting the information you ask for, adds background research and collects this information in one place you can access anytime. Preperation.

&nbsp;

## **Preparation.**

### **What do I want to know?**

To set up an experiment in this field, I had to define what things I want to know, what things I do not know and what things I don’t want to know. I am a very curious person, so this task was relatively easy. I’ve tried to collect questions in a notebook regarding my daily routine to get an idea of the amount of information the assistant had to provide.

### **What provides information?**

The next thing I was wondering about is, what is already providing information? Texture, smells, sounds, visuals? All this and more provides information we understand directly. How can we use this? Can we add context to this information? How do we add context to this information? Could augmented reality provide this context to raw information?

### **What interface is the most convenient in terms of usage and inconspicuousness?**

To find out which way of interaction is the most convenient for me, I tried different kinds of methods.

First, I used Siri with my AirPods which turned out to be nice whenever you are walking, or you are alone. Hands-free, only with your voice. But in public, like in the metro, it felt really awkward to talk to her.

The next thing I worked with was Replika, a chatbot that behaves like a friend and has the approach to motivate you in a daily dialogue to reach goals like to do more sports or to eat healthier. The interaction itself felt more natural than to talk to Siri as the misunderstandings were reduced to a minimum and it felt more private. The only problem I had was that it also felt very awkward to chat with a bot that behaves like a friend from home but wasn’t.

My conclusion is that I focus more on chat than voice. As I experienced, reading what a machine wants to tell you feels way more natural compared to the synthetic voices of today’s assistances, but the machine should still be recognizable as a machine.

The next thing is, you can’t decide if you want to hear something. But you always can decide if you want to see something. Considering this circumstance for interfaces, the interaction via text gives you more freedom if and when you access the
 demanded information.  

### **Example**.

I created an example situation where I showcase how my assistant should work:

*You wake up and go to the kitchen to make a cup of coffee. You realize that you don’t know what coffee actually is. You just ask:*

> **“Emanuel, what is coffee?”**

*Emanuel will now send a notification to your phone. You can read it at the same moment or keep it for the ride to work.*

This example shows how it could be possible to use an assistant to generate a list of things you are curious about. Other examples could be:

&nbsp;

> **“How to divide the trash right?”**
>
> **“Where do the materials for my phone battery come from?”**
>
> **“How much carbon dioxide could  I safe by showering one minute less?”**

&nbsp;

## Prototype.

### **Build & try an assistent.**

The next step was to build this kind of assistant by myself. I got a Raspberry Pi, a microphone, a speaker and two different platforms I wanted to work with.

The first one is called snips. Snipes is providing a web-based toolbox where you can click together the skills for your personal assistant. I did so and uploaded everything on my Pi. It only provides a voice assistant, but they have already announced to release a chatbot soon.

My experience with „Jarvis” (the name of the AI from snips) was frustrating. It was nice and easy to set it up. But it didn’t hold what it promised. It turned out that snips are more focusing on a smart home assistant than an assistant to interact with as the voice of the text to speech synthesizer was really bad and it wasn’t even capable of reading an article from Wikipedia. I will give it another run as soon as they release a new version. But right now, it is not really usable for my case.

The second platform I tried was Mycroft. It is an open-source platform where you can build your assistant in a similar way. Compared to snips it was not so easy to set up. But as it seems it was capable of lot more things. In the end, it didn’t work out as a voice assistant as the hardware configuration I used for snips wasn’t compatible with Mycroft. So, I hooked my machine up to Telegram and so I was able to chat with my machine. The experience was much nicer than with snipes.

The machine had a proper answer to the asked questions. A lot of facts gathered from different sources. Mycroft was way more on the path I could imagine as an assistant.

### **Conclusion.**

This experiment showed me that there are solutions from smaller, privacy focused companies that allow you to build your own assistant. But these assistances are at an early stage. They have to improve a lot to go on the battlefield against the few big players like Alexa, Siri, Duplex or Alibaba’s AI.

### Next steps.

The next steps for my project will be to test more of the many existing AIs which could fit for my purposes. Besides the system, I will focus on how to access available information like Wikipedia or any news channel to use them as a provider. How can I guarantee the quality of information? Can I use an AI to provide that? I will also consider enlarging my field of intervention from the household to outside like the university, sports, shopping etc. I will also do my research on how the atomize gathering information with a monitoring system for the daily routine. There is a project called synthetic sensor by Gierad Laput which allows you to monitor what you are doing in every room. In addition, all of us have a complex sensor in our pocket which can be used for that purpose. I could also imagine taking the Smart Citizen project as an example to take it into our homes which observes your energy consumption, the number of radio waves you are exposed to or how much waste you are producing.



### reflection & feedback.

After the final presentation, my own reflection was that I didn't point out enough that the AI I was working with was only an experiment to see what tools could help to work on my future vision as well as to showcase the status quo of such projects. Most participants focused too much on the product itself and less on my global approach - which obviously was caused by the way I presented my project as the "prototype" was featured the most.

And so the main feedback of the jury was that I should create more scenarios where to use my designed assistant. Also in which context I see this in the future. I also was asked what I would call my assistant in terms of is it a friend like "replica" or is it more a secretary etc.

I was also asked a lot how I could realise this machine with my approach to privacy as the big players nowadays are miles ahead also because of their access to a huge amount of data.

### next steps.

I will work on the connection of my project/product to my overall vision to the future to make it more clear what I am up to. I will also shift my focus from the prototype I have right now to a more tangible example, as many were confused by the application of the machine. I will also rethink the machine-human interaction aspect. I was thinking about the Star Wars saga, as it has drawn the perfect symbiosis between humans and machines. While preparing my final presentation, I also figured out that I would like to put my focus on a decentralised information system that has the approach to challenge the user and so to